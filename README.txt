W,A,S,D,SPACE - MOVE
Arrow keys - move editor (the monkey)
"." and "," - switch model editor
right enter - up editor
right shift - down editor
"[" and "]" - rotate editor
"-" and "+" - scale down/up editor (hold x y or z to scale only on these axis)
m - place model
"~" - console (type help for help)
hold "Ctrl" while scaling, or rotating, the editor to do it in an equal amount of space, angle, scale steps - for example: not 17.241 degrees, but 15 degrees)
How to make level:
Make new text file in folder levels and call it 4.txt
copy first 4 rows of 1.txt or 2.txt or 3.txt (they are the same) and go in game and load it with "loadlevel 4" command in console. then start building with the editor(the monkey with snowman body)
DONT FORGET TO SAVE THE LEVEL WITH savelevel COMMAND!!!!

- Checkpoints: Make sure to always first add the checkpoint flag and
  then the object beneath it.
