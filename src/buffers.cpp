/* Buffers and Strings
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define ARRAY_LEN(arr) (sizeof (arr) / sizeof ((arr)[0]))
#define MALLOC(type, count) (type *) malloc (sizeof (type) * (count))
#define REALLOC(ptr, type, count) (type *) realloc (ptr, sizeof (type) * (count))
#define STRING(cstr) ((String) {(char *) cstr, sizeof (cstr) - 1})


struct Buffer {
  char *data;
  size_t size;
};


struct String {
  char *data;
  size_t size;
};


static Buffer
alloc (size_t size)
{
  Buffer buffer;
  buffer.size = size;
  buffer.data = MALLOC (char, size);
  return buffer;
}


static void
dealloc (Buffer buffer)
{
  free (buffer.data);
}


static Buffer
read_entire_file (const char *filepath)
{
  FILE *file = fopen (filepath, "rb");
  assert (file);

  fseek (file, 0, SEEK_END);
  long file_size = ftell (file);
  assert (file_size >= 0);
  rewind (file);

  Buffer buffer = alloc (file_size);
  size_t bytes_read = fread (buffer.data, 1, file_size, file);
  assert (bytes_read == (size_t) file_size);

  fclose (file);

  return buffer;
}


static String
to_string (const char *cstr)
{
  return {(char *) cstr, strlen (cstr)};
}


static s8
string_cstr_compare (String string, const char *cstr)
{
  for (;; ++cstr)
    {
      char c = cstr[0];

      if (c)
        {
          if (!string.size) return -1;
          if (string.data[0] < c) return -1;
          if (string.data[0] > c) return  1;

          ++string.data;
          --string.size;
        }
      else
        {
          return !!string.size;
        }
    }

  return 0;
}


static s8
compare_strings (String a, String b)
{
  while (a.size && b.size)
    {
      char a_char = a.data[0];
      char b_char = b.data[0];

      if (a_char < b_char) return -1;
      if (a_char > b_char) return  1;

      ++a.data;
      --a.size;
      ++b.data;
      --b.size;
    }

  if (a.size < b.size) return -1;
  if (a.size > b.size) return  1;

  return 0;
}


static b8
operator== (String string, const char *cstr)
{
  for (;; ++cstr)
    {
      char c = cstr[0];

      if (c)
        {
          if (!string.size || c != string.data[0]) return 0;
          ++string.data;
          --string.size;
        }
      else
        {
          return !string.size;
        }
    }

  return 0;
}


static b8
operator!= (String string, const char *cstr)
{
  return !(string == cstr);
}


static size_t
string_get_max_line_len (String string)
{
  size_t max_line_len = 0;
  size_t line_len = 0;

  for (; string.size; --string.size)
    {
      char c = (string.data++)[0];
      if (c == '\n')
        {
          if (line_len > max_line_len) max_line_len = line_len;
          line_len = 0;
        }
      else
        {
          ++line_len;
        }
    }

  if (line_len > max_line_len) max_line_len = line_len;

  return max_line_len;
}


static Buffer
append_char (Buffer buffer, char c)
{
  if (buffer.size)
    {
      buffer.data[0] = c;
      ++buffer.data;
      --buffer.size;
    }
  return buffer;
}


static Buffer
append_string (Buffer buffer, String string)
{
  if (string.size > buffer.size)
    {
      string.size = buffer.size;
      buffer.size = 0;
    }
  else
    {
      buffer.size -= string.size;
    }

  while (string.size)
    {
      buffer.data[0] = string.data[0];
      --string.size;
      ++string.data;
      ++buffer.data;
    }

  return buffer;
}



static String
string_append_string (String dest, String src)
{
  size_t size = dest.size < src.size ? dest.size : src.size;
  dest.size -= size;
  for (; size; --size) (dest.data++)[0] = (src.data++)[0];
  return dest;
}


static String
string_append_char (String string, char c)
{
  if (string.size)
    {
      string.data[0] = c;
      ++string.data;
      --string.size;
    }

  return string;
}


static String
string_append_cstr (String string, const char *s)
{
  for (char c = s[0]; c && string.size; c = (++s)[0])
    {
      string.data[0] = c;
      ++string.data;
      --string.size;
    }
  return string;
}


static String
string_append_u64 (String string, u64 num)
{
  if (num)
    {
      char buffer[20];

      u8 len = 0;
      for (; num; num /= 10) buffer[len++] = num % 10 + '0';

      if (len <= string.size)
        {
          string.size -= len;
          while (len) (string.data++)[0] = buffer[--len];
        }
    }
  else
    {
      if (string.size)
        {
          string.data[0] = '0';
          ++string.data;
          --string.size;
        }
    }

  return string;
}


static String
string_append_s64 (String string, s64 num)
{
  if (string.size)
    {
      if (num)
        {
          if (num < 0)
            {
              num = -num;
              string.data[0] = '-';
              ++string.data;
              --string.size;
            }

          string = string_append_u64 (string, num);
        }
      else
        {
          string.data[0] = '0';
          ++string.data;
          --string.size;
        }
    }

  return string;
}


static String
string_append_formatted (String string, const char *format, va_list args)
{
  for (char c = format[0]; c && string.size; c = (++format)[0])
    {
      if (c == '%')
        {
          c = (++format)[0];
          if (c == 0)
            {
              string = string_append_char (string, '%');
              break;
            }

          switch (c)
            {
            case 's': string = string_append_cstr (string, va_arg (args, char *)); break;
            // case 'f': string = string_append_r64  (string, va_arg (args, double)); break;
            case 'd': string = string_append_s64  (string, va_arg (args, int)); break;
            case 'u': string = string_append_u64  (string, va_arg (args, unsigned)); break;
            case '%': string = string_append_char (string, '%'); break;
            default:
              string = string_append_char (string, '%');
              string = string_append_char (string, c);
            }
        }
      else
        {
          string = string_append_char (string, c);
        }
    }

  return string;
}


static u64
string_get_u64 (String *string)
{
  String p = *string;
  u64 num = 0;

  while (p.size)
    {
      u8 c = p.data[0];
      if (c < '0' || c > '9') break;
      ++p.data;
      --p.size;

      num *= 10;
      num += c - '0';
    }

  *string = p;

  return num;
}


static r32
string_get_r32 (String *string)
{
  char cstring[string->size + 1];
  memcpy (cstring, string->data, string->size);
  cstring[string->size] = 0;

  char *end;
  r32 num = strtof (cstring, &end);
  if (end == cstring) return 0;

  size_t parsed_size = end - cstring;
  string->data += parsed_size;
  string->size -= parsed_size;

  return num;
}
