/* GNU/Linux Utils
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <netinet/ip.h>         // AF_INET, sockaddr_in
#include <arpa/inet.h>          // htons, htonl
#include <netdb.h>              // getaddrinfo

#include <dirent.h>
#include <pthread.h>


int         remote_fd = -1;
sockaddr_in remote_addr;
socklen_t   remote_addr_len = sizeof (sockaddr_in);


static String
os_recv (Buffer buffer)
{
  ssize_t recv_status = recvfrom (remote_fd, buffer.data, buffer.size, 0, (sockaddr *) &remote_addr, &remote_addr_len);

  if (recv_status == -1)
    {
      int error = errno;
      push_network_message (to_string (strerror (error)));
      return {};
    }
  else
    {
      u32 bytes_recv = recv_status;
      network.bytes_recv += bytes_recv;
      String data = {buffer.data, bytes_recv};
      return data;
    }
}


static u32
os_send (String data)
{
  ssize_t send_status = sendto (remote_fd, data.data, data.size, 0, (sockaddr *) &remote_addr, sizeof (remote_addr));
  if (send_status == -1)
    {
      int error = errno;
      push_network_message (to_string (strerror (error)));
      return 0;
    }
  else
    {
      u32 bytes_sent = (u32) send_status;
      network.bytes_sent += bytes_sent;
      return bytes_sent;
    }
}


static void
ip4_listen (void)
{
  remote_fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (remote_fd == -1)
    {
      push_network_message (STRING ("socket error"));
      return;
    }

  sockaddr_in local_addr = {};
  local_addr.sin_family = AF_INET;
  local_addr.sin_port = htons (9112);
  local_addr.sin_addr.s_addr = htonl (INADDR_ANY);

  int bind_error = bind (remote_fd, (sockaddr *) &local_addr, sizeof (local_addr));
  if (bind_error)
    {
      close (remote_fd);
      push_network_message (STRING ("bind error"));
      return;
    }
}


static b8
ip4_connect (String address)
{
  b8 result = 0;

  char address_cstr[address.size + 1];
  memcpy (address_cstr, address.data, address.size);
  address_cstr[address.size] = 0;

  addrinfo address_info_hints = {};
  address_info_hints.ai_flags = AI_NUMERICSERV;
  address_info_hints.ai_family = AF_INET;
  address_info_hints.ai_socktype = SOCK_DGRAM;
  address_info_hints.ai_protocol = IPPROTO_UDP;

  addrinfo *address_info_result = NULL;
  int getaddrinfo_error = getaddrinfo (address_cstr, "9112", &address_info_hints, &address_info_result);
  if (getaddrinfo_error)
    {
      push_network_message (to_string (gai_strerror (getaddrinfo_error)));
    }
  else
    {
      remote_fd = socket (address_info_result->ai_family, address_info_result->ai_socktype, address_info_result->ai_protocol);

      if (remote_fd == -1)
        {
          int error = errno;
          push_network_message (to_string (strerror (error)));
        }
      else
        {
          remote_addr = ((sockaddr_in *) address_info_result->ai_addr)[0];
          result = 1;
        }

      freeaddrinfo (address_info_result);
    }

  return result;
}


static void *
start_network_connection_threaded (void *thread_args)
{
  start_network_connection ();
  return NULL;
}


static void
start_network_connection_thread (void)
{
  pthread_t thread;
  int thread_error = pthread_create (&thread, NULL, start_network_connection_threaded, NULL);
  assert (!thread_error);
}


static size_t
recv_netlink_message (int sock_fd, iovec iov)
{
  msghdr msg = {};
  // msg.msg_name    = &addr;
  // msg.msg_namelen = sizeof (sockaddr_nl);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;

  ssize_t bytes_got = recvmsg (sock_fd, &msg, 0);
  assert (bytes_got >= 0);

  if (msg.msg_flags & MSG_TRUNC) fprintf (stderr, "Error: Message was truncated.\n");
  assert (!(msg.msg_flags & MSG_TRUNC));

  return bytes_got;
}


static void
send_rtnetlink_getaddr_message (int sock_fd)
{
  sockaddr_nl dest_addr = {};
  dest_addr.nl_family = AF_NETLINK;

  char nl_msg_buffer[NLMSG_SPACE (sizeof (rtmsg))] = {};
  nlmsghdr *nl_msg = (nlmsghdr *) nl_msg_buffer;
  nl_msg->nlmsg_len   = NLMSG_LENGTH (sizeof (rtmsg));
  nl_msg->nlmsg_type  = RTM_GETADDR;
  nl_msg->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;

  ifaddrmsg *ifaddr_msg = (ifaddrmsg *) NLMSG_DATA (nl_msg);
  ifaddr_msg->ifa_family = AF_INET;

  iovec iov = {nl_msg, sizeof (nl_msg_buffer)};

  msghdr msg = {};
  msg.msg_name    = &dest_addr;
  msg.msg_namelen = sizeof (sockaddr_nl);
  msg.msg_iov     = &iov;
  msg.msg_iovlen  = 1;

  ssize_t bytes_sent = sendmsg (sock_fd, &msg, 0);
  assert (bytes_sent >= 0);
}


static NetworkInterface
get_network_inteface (nlmsghdr *nl_response)
{
  ifaddrmsg *ifa = (ifaddrmsg *) NLMSG_DATA (nl_response);

  NetworkInterface net_interface = {};
  net_interface.index = ifa->ifa_index;

  ssize_t attr_len = IFA_PAYLOAD (nl_response);
  for (rtattr *attr = IFA_RTA (ifa);
       RTA_OK (attr, attr_len);
       attr = RTA_NEXT (attr, attr_len))
    {
      switch (attr->rta_type)
        {
        case IFA_ADDRESS:
          net_interface.ip4_address_u32 = ((u32 *) RTA_DATA (attr))[0];
          break;
        case IFA_LABEL:
          char *label = (char *) RTA_DATA (attr);
          assert (strlen (label) < sizeof (net_interface.label));
          strcpy (net_interface.label, label);
          break;
        }
    }

  return net_interface;
}


static void
update_network_interface (NetworkInterface nif)
{
  for (u32 i = 0; i < network.interfaces.count; ++i)
    {
      if (network.interfaces.list[i].index == nif.index)
        {
          network.interfaces.list[i] = nif;
          return;
        }
    }

  assert (network.interfaces.count < network.interfaces.max);
  network.interfaces.list[network.interfaces.count++] = nif;
}


static void
remove_network_interface (NetworkInterface nif)
{
  for (u32 i = 0; i < network.interfaces.count; ++i)
    {
      if (network.interfaces.list[i].index == nif.index)
        {
          network.interfaces.list[i] = network.interfaces.list[--network.interfaces.count];
          return;
        }
    }
}


static void *
watch_network_changes (void *args)
{
  int sock_fd = socket (AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE);
  assert (sock_fd >= 0);

  sockaddr_nl src_addr = {};
  src_addr.nl_family = AF_NETLINK;
  src_addr.nl_groups = RTMGRP_IPV4_IFADDR;
  int bind_error = bind (sock_fd, (sockaddr *) &src_addr, sizeof (src_addr));
  assert (!bind_error);

  send_rtnetlink_getaddr_message (sock_fd);

  while (1)
    {
      char   nl_response_buffer[8192];
      size_t nl_response_len = recv_netlink_message (sock_fd, {nl_response_buffer, sizeof (nl_response_buffer)});

      for (nlmsghdr *nl_response = (nlmsghdr *) nl_response_buffer;
           NLMSG_OK (nl_response, nl_response_len);
           nl_response = NLMSG_NEXT (nl_response, nl_response_len))
        {
          switch (nl_response->nlmsg_type)
            {
            case RTM_NEWADDR:
              update_network_interface (get_network_inteface (nl_response));
              break;
            case RTM_DELADDR:
              remove_network_interface (get_network_inteface (nl_response));
              break;
            }
        }
    }

  return NULL;
}


static void
start_networking (void)
{
  pthread_t watch_network_thread;
  int watch_network_thread_error = pthread_create (&watch_network_thread, NULL, watch_network_changes, NULL);
  assert (!watch_network_thread_error);
}


static void
list_models (void)
{
  console_print ("Doesn't work for linux - someone should implement this :/\n");
}


static b8
list_levels (void)
{
  DIR *dir = opendir (LEVELS_DIRPATH);
  if (!dir) return 0;

  String buffer;
  buffer.size = 8192;
  char _buffer[buffer.size];
  buffer.data = _buffer;

  size_t level_names_max = 128;
  String level_names[level_names_max];
  size_t level_names_count = 0;

  while (1)
    {
      dirent *entry = readdir (dir);
      if (!entry) break;

      String filename;
      filename.data = entry->d_name;
      if (filename.data[0] == '.') continue;
      filename.size = strlen (filename.data);
      if (filename.size <= 4) continue;
      filename.size -= 4;

      if (strcmp (filename.data + filename.size, ".txt")) continue;

      if (filename.size > buffer.size)
        {
          console_print ("Error: Filename buffer is full - some levels won't be listed.\n");
          break;
        }
      if (level_names_count >= level_names_max)
        {
          console_print ("Error: Level names buffer is full - some levels won't be listed.\n");
          break;
        }

      String level_name;
      level_name.data = buffer.data;
      level_name.size = filename.size;
      buffer = string_append_string (buffer, filename);

      level_names[level_names_count++] = level_name;
    }

  for (size_t len = level_names_count - 1; len; --len)
    {
      b8 swap = 0;

      for (size_t i = 0; i < len; ++i)
        {
          if (compare_strings (level_names[i], level_names[i+1]) > 0)
            {
              swap = 1;
              String level_name = level_names[i];
              level_names[i] = level_names[i+1];
              level_names[i+1] = level_name;
            }
        }

      if (!swap) break;
    }

  for (size_t i = 0; i < level_names_count; ++i)
    {
      console_print (level_names[i]);
      console_print ("\n");
    }

  return 1;
}


static void
load_models (const char *dirpath)
{
  DIR *dir = opendir (dirpath);
  assert (dir);

  assets.models = MALLOC (Model, assets.models_max);
  size_t dirpath_len = strlen (dirpath);

  while (1)
    {
      dirent *entry = readdir (dir);
      if (!entry) break;

      char *filename = entry->d_name;
      if (filename[0] == '.') continue;

      size_t filename_len = strlen (filename);

      if (filename_len > 4 && !strcmp (filename + (filename_len - 4), ".obj"))
        {
          size_t filepath_len = dirpath_len + 1 + filename_len;
          char   filepath[filepath_len + 1];
          strcpy (filepath, dirpath);
          strcat (filepath, "/");
          strcat (filepath, filename);
          assets.models[assets.models_count++] = load_model (filepath, filename);
        }
    }

  closedir (dir);
}
