/* Assets
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


struct Vertex {
  V3 pos;
  V3 normal;
  V3 color;
};


struct Triangle {
  Vertex v[3];
};


struct Texture {
  int width;
  int height;
  GLuint gl_id;
};


struct Font {
  u32 glyph_w;
  u32 glyph_h;
  u32 glyph_count;
  Texture foreground;
  Texture background;
  GLuint  foreground_rect_array_buffer;
  GLuint  background_rect_array_buffer;
  int watch_id;
  b8 changed;
  const char *filepath;
};


struct Material {
  char *name;
  V3 color;
};


struct Materials {
  Material *list;
  u32 count;
};


struct ShaderProgram {
  GLuint gl_id;

  GLint position_attrib;
  GLint normal_attrib;
  GLint color_attrib;
  GLint texcoord_attrib;

  GLint transform_uni;
  GLint color_uni;
  GLint texture_uni;
  GLint texcoord_xoffset_uni;
};


struct Assets {
  Font font;
  ShaderProgram main_shader;
  ShaderProgram font_shader;

  u32     models_max = 64;
  u32     models_count;
  Model  *models;
  Model   missing_model;
} assets;


static void
load_font_data (Font *font)
{
  printf ("load font data: %s\n", font->filepath);
  size_t foreground_datasize = font->foreground.width * font->foreground.height;
  size_t background_datasize = font->background.width * font->background.height;

  FILE *foreground_file = fopen (font->filepath, "rb");
  assert (foreground_file);

  u8 *foreground_data = MALLOC (u8, foreground_datasize);
  size_t bytes_read = fread (foreground_data, 1, foreground_datasize, foreground_file);
  assert (bytes_read == foreground_datasize);

  fclose (foreground_file);

  u8 *background_data = (u8 *) calloc (1, background_datasize);

  for (int y = 0; y < font->foreground.height; ++y)
    {
      u8 *row = foreground_data + (y * font->foreground.width);
      for (int x = 0; x < font->foreground.width; ++x)
        {
          if (row[x])
            {
              int glyph = x / font->glyph_w;
              int bg_x = x + glyph * 2;
              background_data[(y+0) * font->background.width + bg_x+0] = 0xff;
              background_data[(y+0) * font->background.width + bg_x+1] = 0xff;
              background_data[(y+0) * font->background.width + bg_x+2] = 0xff;
              background_data[(y+1) * font->background.width + bg_x+0] = 0xff;
              background_data[(y+1) * font->background.width + bg_x+1] = 0xff;
              background_data[(y+1) * font->background.width + bg_x+2] = 0xff;
              background_data[(y+2) * font->background.width + bg_x+0] = 0xff;
              background_data[(y+2) * font->background.width + bg_x+1] = 0xff;
              background_data[(y+2) * font->background.width + bg_x+2] = 0xff;
            }
        }
    }

  glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
  glPixelTransferf (GL_RED_BIAS,   1.0f);
  glPixelTransferf (GL_GREEN_BIAS, 1.0f);
  glPixelTransferf (GL_BLUE_BIAS,  1.0f);

  glGenTextures (1, &font->foreground.gl_id);
  glBindTexture (GL_TEXTURE_2D, font->foreground.gl_id);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                font->foreground.width, font->foreground.height, 0,
                GL_ALPHA, GL_UNSIGNED_BYTE,
                foreground_data);

  glGenTextures (1, &font->background.gl_id);
  glBindTexture (GL_TEXTURE_2D, font->background.gl_id);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                font->background.width, font->background.height, 0,
                GL_ALPHA, GL_UNSIGNED_BYTE,
                background_data);

  glPixelStorei (GL_UNPACK_ALIGNMENT, 4);
  glPixelTransferf (GL_RED_BIAS,   0.0f);
  glPixelTransferf (GL_GREEN_BIAS, 0.0f);
  glPixelTransferf (GL_BLUE_BIAS,  0.0f);

  // printf ("load_font_data (%s):\n", font->filepath);
  // debug_print (font->foreground.gl_id);
  // debug_print (font->background.gl_id);

  free (foreground_data);
  free (background_data);
}


static Font
load_font (const char *filepath, u32 glyph_w, u32 glyph_h, u32 glyph_count)
{
  Font font = {};
  font.glyph_w = glyph_w;
  font.glyph_h = glyph_h;
  font.glyph_count = glyph_count;
  font.filepath = filepath;
  // font.watch_id = inotify_add_watch (state.inotify_fd, font.filepath, IN_CLOSE_WRITE);
  // debug_print (font.watch_id);

  font.foreground.width  = font.glyph_w * font.glyph_count;
  font.foreground.height = font.glyph_h;
  font.background.width  = (font.glyph_w + 2) * font.glyph_count;
  font.background.height = (font.glyph_h + 2);

  load_font_data (&font);

  r32 tx1 = 1.0f / font.glyph_count;

  r32 fg_x1 = font.glyph_w;
  r32 fg_y1 = font.glyph_h;
  r32 foreground_rect[] = {
    0,     0,    0, 1,
    fg_x1,     0,  tx1, 1,
    0, fg_y1,    0, 0,
    fg_x1, fg_y1,  tx1, 0,
  };

  glGenBuffers (1, &font.foreground_rect_array_buffer);
  glBindBuffer (GL_ARRAY_BUFFER, font.foreground_rect_array_buffer);
  glBufferData (GL_ARRAY_BUFFER, sizeof (foreground_rect), foreground_rect, GL_STATIC_DRAW);

  r32 bg_x1 = font.glyph_w + 1;
  r32 bg_y1 = font.glyph_h + 1;
  r32 background_rect[] = {
    -1,    -1,    0, 1,
    bg_x1,    -1,  tx1, 1,
    -1, bg_y1,    0, 0,
    bg_x1, bg_y1,  tx1, 0,
  };

  glGenBuffers (1, &font.background_rect_array_buffer);
  glBindBuffer (GL_ARRAY_BUFFER, font.background_rect_array_buffer);
  glBufferData (GL_ARRAY_BUFFER, sizeof (background_rect), background_rect, GL_STATIC_DRAW);

  return font;
}


static void
reload_font (Font *font)
{
  assert (glIsTexture (font->foreground.gl_id));
  assert (glIsTexture (font->background.gl_id));
  glDeleteTextures (1, &font->background.gl_id);
  glDeleteTextures (1, &font->foreground.gl_id);
  load_font_data (font);
}


static GLuint
load_shader (const char *filepath, GLuint shader_type)
{
  Buffer source = read_entire_file (filepath);

  GLuint shader = glCreateShader (shader_type);
  GLint source_length = source.size;
  glShaderSource (shader, 1, (const char **) &source.data, &source_length);
  glCompileShader (shader);

  dealloc (source);

  GLint compile_status;
  glGetShaderiv (shader, GL_COMPILE_STATUS, &compile_status);

  GLint info_log_length;
  glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &info_log_length);

  if (info_log_length > 1)
    {
      char info_log[info_log_length];
      glGetShaderInfoLog (shader, info_log_length, 0, info_log);
      printf ("%s: %s", filepath, info_log);
    }

  assert (compile_status);

  return shader;
}


static ShaderProgram
load_shader_program (const char *vshader_filepath, const char *fshader_filepath)
{
  GLuint vshader = load_shader (vshader_filepath, GL_VERTEX_SHADER);
  GLuint fshader = load_shader (fshader_filepath, GL_FRAGMENT_SHADER);

  ShaderProgram program;
  program.gl_id = glCreateProgram ();
  glAttachShader (program.gl_id, vshader);
  glAttachShader (program.gl_id, fshader);
  glLinkProgram (program.gl_id);

  // TODO: Check for link errors
  // TODO: Maybe delete shaders?
  //       glDeleteShader (vshader)
  //       glDeleteShader (fshader)

  program.position_attrib = glGetAttribLocation  (program.gl_id, "position");
  program.texcoord_attrib = glGetAttribLocation  (program.gl_id, "texcoord");
  program.normal_attrib   = glGetAttribLocation  (program.gl_id, "normal");
  program.color_attrib    = glGetAttribLocation  (program.gl_id, "color");

  program.transform_uni        = glGetUniformLocation (program.gl_id, "transform_uni");
  program.color_uni            = glGetUniformLocation (program.gl_id, "color_uni");
  program.texture_uni          = glGetUniformLocation (program.gl_id, "texture_uni");
  program.texcoord_xoffset_uni = glGetUniformLocation (program.gl_id, "texcoord_xoffset_uni");

  return program;
}


static Materials
load_materials (const char *filepath)
{
  FILE *file = fopen (filepath, "r");
  assert (file);

  Materials materials = {};
  u32 materials_max = 16;
  materials.list = MALLOC (Material, materials_max);

  while (1)
    {
      char word[128];
      int word_scanned = fscanf (file, "%s", word);
      if (word_scanned == EOF) break;

      if (!strcmp (word, "newmtl"))
        {
          assert (materials.count < materials_max);
          fscanf (file, "%s", word);
          Material material = {};
          material.name = MALLOC (char, strlen (word) + 1);
          strcpy (material.name, word);
          materials.list[materials.count++] = material;
        }
      else if (!strcmp (word, "Kd"))
        {
          r32 r, g ,b;
          fscanf (file, "%f %f %f", &r, &g, &b);
          materials.list[materials.count - 1].color = (V3){r, g, b};
        }
      else if (!strcmp (word, "#") ||
               !strcmp (word, "Ns") ||
               !strcmp (word, "Ni") ||
               !strcmp (word, "Ka") ||
               !strcmp (word, "Ks") ||
               !strcmp (word, "Ke") ||
               !strcmp (word, "d") ||
               !strcmp (word, "illum"))
        {
          fgets (word, 128, file);
        }
      else
        {
          fprintf (stderr, "Error: Unknown MTL line token: %s\n", word);
          fgets (word, 128, file);
        }
    }

  fclose (file);

  return materials;
}


static Model
load_model (const char *filepath, const char *name)
{
  size_t name_len = strlen (name);
  assert (name_len > 4);
  assert (!strcmp (name_len - 4 + name, ".obj"));

  Model model = {};
  model.name.size = name_len - 4;
  model.name.data = MALLOC (char, model.name.size);
  memcpy (model.name.data, name, model.name.size);

  FILE *file = fopen (filepath, "r");
  assert (file);

  const char *filename = strrchr (filepath, '/');
  size_t dirpath_len = filename ? filename + 1 - filepath : 0;

  static u32 positions_max = 4096;
  static u32 normals_max   = 4096;
  static u32 triangles_max = 4096;
  static V3 *positions = MALLOC (V3, positions_max);
  static V3 *normals   = MALLOC (V3, normals_max);
  static Triangle *triangles = MALLOC (Triangle, triangles_max);
  u32 positions_count = 0;
  u32 normals_count   = 0;
  u32 triangles_count = 0;

  Materials materials = {};
  Material *current_material = 0;

  CollisionBox  collision_boxes[256];
  u32           collision_boxes_max = ARRAY_LEN (collision_boxes);
  CollisionBox *collision_box = NULL;
  b8 collision_box_first_pos;
  V3 collision_box_min;
  V3 collision_box_max;

  while (1)
    {
      char word[128];
      int word_scanned = fscanf (file, "%s", word);
      if (word_scanned == EOF) break;

      if (!strcmp (word, "f"))
        {
          u32 vector_indexes[3];
          u32 normal_indexes[3];
          int scanned_items = fscanf (file, "%u//%u %u//%u %u//%u",
                                      vector_indexes + 0, normal_indexes + 0,
                                      vector_indexes + 1, normal_indexes + 1,
                                      vector_indexes + 2, normal_indexes + 2);
          assert (scanned_items == 6);

          if (!collision_box)
            {
              if (triangles_count >= triangles_max)
                {
                  triangles_max *= 2;
                  assert (triangles_count < triangles_max);
                  triangles = REALLOC (triangles, Triangle, triangles_max);
                }

              Triangle t;
              t.v[0].pos    = positions[vector_indexes[0] - 1];
              t.v[0].normal = normals  [normal_indexes[0] - 1];
              t.v[0].color  = current_material->color;
              t.v[1].pos    = positions[vector_indexes[1] - 1];
              t.v[1].normal = normals  [normal_indexes[1] - 1];
              t.v[1].color  = current_material->color;
              t.v[2].pos    = positions[vector_indexes[2] - 1];
              t.v[2].normal = normals  [normal_indexes[2] - 1];
              t.v[2].color  = current_material->color;
              triangles[triangles_count++] = t;
            }
        }
      else if (!strcmp (word, "v"))
        {
          V3 position;
          int scanned_items = fscanf (file, "%f %f %f", &position.x, &position.y, &position.z);
          assert (scanned_items == 3);

          if (collision_box)
            {
              if (collision_box_first_pos)
                {
                  collision_box_first_pos = 0;
                  collision_box_min = position;
                  collision_box_max = position;
                }
              else
                {
                  if (position.x < collision_box_min.x) collision_box_min.x = position.x;
                  if (position.y < collision_box_min.y) collision_box_min.y = position.y;
                  if (position.z < collision_box_min.z) collision_box_min.z = position.z;
                  if (position.x > collision_box_max.x) collision_box_max.x = position.x;
                  if (position.y > collision_box_max.y) collision_box_max.y = position.y;
                  if (position.z > collision_box_max.z) collision_box_max.z = position.z;
                }
            }

          if (positions_count >= positions_max)
            {
              positions_max *= 2;
              assert (positions_count < positions_max);
              positions = REALLOC (positions, V3, positions_max);
            }

          positions[positions_count++] = position;
        }
      else if (!strcmp (word, "vn"))
        {
          V3 normal;
          int scanned_items = fscanf (file, "%f %f %f", &normal.x, &normal.y, &normal.z);
          assert (scanned_items == 3);

          if (normals_count >= normals_max)
            {
              normals_max *= 2;
              assert (normals_count < normals_max);
              normals = REALLOC (normals, V3, normals_max);
            }

          normals[normals_count++] = normal;
        }
      else if (!strcmp (word, "mtllib"))
        {
          fscanf (file, "%s", word);
          size_t word_len = strlen (word);
          size_t materials_filepath_len = dirpath_len + word_len;
          char   materials_filepath[materials_filepath_len + 1];
          memcpy (materials_filepath, filepath, dirpath_len);
          memcpy (materials_filepath + dirpath_len, word, word_len);
          materials_filepath[materials_filepath_len] = 0;
          materials = load_materials (materials_filepath);
        }
      else if (!strcmp (word, "usemtl"))
        {
          fscanf (file, "%s", word);
          current_material = 0;
          for (u32 i = 0; i < materials.count; i++)
            {
              if (!strcmp (materials.list[i].name, word))
                {
                  current_material = materials.list + i;
                  break;
                }
            }

          if (!current_material)
            {
              fprintf (stderr, "Error: Can't find material '%s' for object '%s'\n", word, name);
            }
        }
      else if (!strcmp (word, "o"))
        {
          fgets (word, 128, file);

          if (collision_box)
            {
              collision_box->pos = (collision_box_min + collision_box_max) / 2;
              collision_box->dim = collision_box_max - collision_box_min;
              collision_box = NULL;
            }

          if (!memcmp (word, " collision_box_", 15))
            {
              assert (model.collision_boxes_count < collision_boxes_max);
              collision_box = collision_boxes + model.collision_boxes_count++;
              collision_box_first_pos = 1;
            }
        }
      else if (!strcmp (word, "#") ||
               !strcmp (word, "s"))
        {
          fgets (word, 128, file);
        }
      else
        {
          fprintf (stderr, "Error: Unknown OBJ line token: %s\n", word);
          fgets (word, 128, file);
        }
    }

  if (collision_box)
    {
      collision_box->pos = (collision_box_min + collision_box_max) / 2;
      collision_box->dim = collision_box_max - collision_box_min;
    }

  if (model.collision_boxes_count)
    {
      model.collision_boxes = MALLOC (CollisionBox, model.collision_boxes_count);
      for (u32 i = 0; i < model.collision_boxes_count; i++)
        {
          model.collision_boxes[i] = collision_boxes[i];
        }
    }

  model.vertices_count = triangles_count * 3;

  glGenBuffers (1, &model.array_buffer);
  glBindBuffer (GL_ARRAY_BUFFER, model.array_buffer);
  glBufferData (GL_ARRAY_BUFFER, triangles_count * sizeof (triangles[0]), triangles, GL_STATIC_DRAW);

  return model;
}


static Model *
get_model_by_name (const char *name)
{
  size_t name_len = strlen (name);
  for (u32 i = 0; i < assets.models_count; ++i)
    {
      Model *model = assets.models + i;

      if (name_len == model->name.size && !memcmp (model->name.data, name, name_len))
        {
          return model;
        }
    }

  printf ("get_model_by_name: Can't find: %s\n", name);
  return &assets.missing_model;

  return 0;
}


static FILE *
open_level_file (String level_name, const char *fopen_mode)
{
  if (level_name.size > sizeof (game_state.level_name_buffer))
    {
      console_print ("Error: Level name is too long: ");
      console_print (level_name);
      console_print ("\n");
      return 0;
    }

  const char *dirpath = LEVELS_DIRPATH;
  size_t      dirpath_len = strlen (dirpath);
  const char *extension = ".txt";
  size_t      extension_len = strlen (extension);

  size_t filepath_len = dirpath_len + 1 + level_name.size + extension_len;
  char   filepath[filepath_len + 1];
  memcpy (filepath, dirpath, dirpath_len);
  filepath[dirpath_len] = '/';
  memcpy (filepath + dirpath_len + 1, level_name.data, level_name.size);
  memcpy (filepath + dirpath_len + 1 + level_name.size, extension, extension_len);
  filepath[filepath_len] = 0;

  FILE *file = fopen (filepath, fopen_mode);
  if (!file)
    {
      console_print ("Error: Can't open level file: ");
      console_print (filepath);
      console_print ("\n");
    }
  else
    {
      game_state.level_name.data = game_state.level_name_buffer;
      game_state.level_name.size = level_name.size;
      string_append_string (game_state.level_name, level_name);
    }

  return file;
}


static b8
load_level (String level_name)
{
  FILE *file = open_level_file (level_name, "r");
  if (!file) return 0;

  // TODO: Free unused models?
  if (!game_state.objects)
    {
      game_state.objects = MALLOC (Object, game_state.objects_max);
    }
  game_state.objects_count = 0;

  while (1)
    {
      char name[128];
      int scanned_items = fscanf (file, "%s\n", name);
      if (scanned_items != 1) break;

      Object object = {};
      object.model = get_model_by_name (name);

      if (strcmp (name, "earth") == 0)
        {
          object.rot_anim_speed = (V3){0, 0.1, 0};
        }

      if (strcmp (name, "gun") == 0)
        {
          object.rot_anim_speed = (V3){0, 0.1, 0};
        }

      fscanf (file, "%f %f %f\n", &object.pos.x, &object.pos.y, &object.pos.z);
      fscanf (file, "%f %f %f\n", &object.rot.x, &object.rot.y, &object.rot.z);
      fscanf (file, "%f %f %f\n", &object.scale.x, &object.scale.y, &object.scale.z);

      game_state.objects[game_state.objects_count++] = object;
    }

  fclose (file);

  game_state.level_times[0] = (u32) -1;
  game_state.level_times[1] = (u32) -1;
  game_state.level_times[2] = (u32) -1;
  game_state.current_level_time = 0;

  return 1;
}


static b8
save_level (String level_name)
{
  FILE *file = open_level_file (level_name, "w");
  if (!file) return 0;

  for(u32 i = 0; i < game_state.objects_count; i++)
    {
      Object o = game_state.objects[i];
      fprintf (file, "%.*s\n", (int) o.model->name.size, o.model->name.data);
      fprintf (file, "%9f %9f %9f\n", o.pos.x,   o.pos.y,   o.pos.z);
      fprintf (file, "%9f %9f %9f\n", o.rot.x,   o.rot.y,   o.rot.z);
      fprintf (file, "%9f %9f %9f\n", o.scale.x, o.scale.y, o.scale.z);
    }

  fclose(file);

  return 1;
}

