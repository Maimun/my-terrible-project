/* Windows NT Utils
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Winsock2.h>
#include <Ws2tcpip.h> // getaddrinfo
#include <Windows.h>
#include <iphlpapi.h>


SOCKET      remote_fd = INVALID_SOCKET;
sockaddr_in remote_addr;
int         remote_addr_len = sizeof (remote_addr);


static String
os_recv (Buffer buffer)
{
  int recv_status = recvfrom (remote_fd, (char*) buffer.data, buffer.size, 0, (sockaddr*)&remote_addr, &remote_addr_len);

 if (recv_status == SOCKET_ERROR)
    {
      // int error = WSAGetLastError ();
      // TODO: use FormatMessage function to get better info on the error.
      push_network_message (STRING ("recvfrom error"));
      return {};
    }
  else
    {
      u32 bytes_recv = recv_status;
      network.bytes_recv += bytes_recv;
      String data = {buffer.data, bytes_recv};
      return data;
    }
}


static u32
os_send (String data)
{
  int send_status = sendto (remote_fd, data.data, data.size, 0, (sockaddr*)&remote_addr, remote_addr_len);
  if (send_status == SOCKET_ERROR)
    {
      // int error = WSAGetLastError ();
      // TODO: use FormatMessage function to get better info on the error.
      push_network_message (STRING ("sendto error"));
      return 0;
    }
  else
    {
      u32 bytes_sent = (u32) send_status;
      network.bytes_sent += bytes_sent;
      return bytes_sent;
    }
}


static void
ip4_listen (void)
{
  remote_fd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (remote_fd == INVALID_SOCKET)
    {
      // int error = WSAGetLastError ();
      // TODO: use FormatMessage function to get better info on the error.
      push_network_message (STRING ("socket error"));
      return;
    }

  sockaddr_in address = {};
  address.sin_family = AF_INET;
  address.sin_port = htons(9112);
  address.sin_addr.s_addr = htonl(INADDR_ANY);

  int bind_error = bind (remote_fd, (sockaddr *) &address, sizeof (address));
  if (bind_error == SOCKET_ERROR)
    {
      // int error = WSAGetLastError ();
      // TODO: use FormatMessage function to get better info on the error.
      push_network_message (STRING ("bind error"));
      closesocket (remote_fd);
      return;
    }
}


static b8
ip4_connect (String address)
{
  b8 result = 0;

  char address_cstr[address.size + 1];
  memcpy (address_cstr, address.data, address.size);
  address_cstr[address.size] = 0;

  addrinfo address_info_hints = {};
  address_info_hints.ai_flags = AI_NUMERICSERV;
  address_info_hints.ai_family = AF_INET;
  address_info_hints.ai_socktype = SOCK_DGRAM;
  address_info_hints.ai_protocol = IPPROTO_UDP;

  addrinfo *address_info_result = NULL;
  int getaddrinfo_error = getaddrinfo (address_cstr, "9112", &address_info_hints, &address_info_result);
  if (getaddrinfo_error)
    {
      // TODO: Use WSAGetLastError instead of gai_strerror, because of
      // thread safety.
      push_network_message (to_string (gai_strerror (getaddrinfo_error)));
    }
  else
    {
      remote_fd = socket (address_info_result->ai_family, address_info_result->ai_socktype, address_info_result->ai_protocol);
      if (remote_fd == INVALID_SOCKET)
        {
          // int error = WSAGetLastError ();
          // TODO: use FormatMessage function to get better info on the error.
          push_network_message (STRING ("socket error"));
        }
      else
        {
          remote_addr = ((sockaddr_in *) address_info_result->ai_addr)[0];
          result = 1;
        }

      freeaddrinfo (address_info_result);
    }

  return result;
}


static DWORD
start_network_connection_threaded (void *thread_args)
{
  start_network_connection ();
  return 0;
}


static void
start_network_connection_thread (void)
{
  HANDLE thread = CreateThread (NULL, 0, start_network_connection_threaded, NULL, 0, NULL);
  assert (thread);
}


static void
start_networking (void)
{
  WSADATA wsaData;
  int startup_error = WSAStartup (MAKEWORD (2,2), &wsaData);
  if (startup_error != 0)
    {
      console_print ("WSAStartup failed: %d\n", startup_error);
      return;
    }

  /*
  ULONG interface_info_buffer_size = 0;
  DWORD get_interface_info_result = GetInterfaceInfo (NULL, &interface_info_buffer_size);
  assert (get_interface_info_result == ERROR_INSUFFICIENT_BUFFER);
  PIP_INTERFACE_INFO interface_info = (PIP_INTERFACE_INFO) malloc (interface_info_buffer_size);
  get_interface_info_result = GetInterfaceInfo(interface_info, &interface_info_buffer_size);
  assert (get_interface_info_result == NO_ERROR);
  printf ("adapters_count: %ld\n", interface_info->NumAdapters);
  for (LONG i = 0; i < interface_info->NumAdapters; i++)
    {
      printf ("%lu: %ls\n", interface_info->Adapter[i].Index, interface_info->Adapter[i].Name);
    }
  */

  ULONG ipaddr_table_buffer_size = 0;
  DWORD get_ipaddr_table_result = GetIpAddrTable (NULL, &ipaddr_table_buffer_size, 0);
  assert (get_ipaddr_table_result == ERROR_INSUFFICIENT_BUFFER);
  PMIB_IPADDRTABLE ipaddr_table = (PMIB_IPADDRTABLE) malloc (ipaddr_table_buffer_size);
  get_ipaddr_table_result = GetIpAddrTable(ipaddr_table, &ipaddr_table_buffer_size, 0);
  assert (get_ipaddr_table_result == NO_ERROR);
  //printf ("tables_count: %ld\n", ipaddr_table->dwNumEntries);
  for (DWORD i = 0; i < ipaddr_table->dwNumEntries; i++)
    {
      //printf ("%lu: %lx\n", ipaddr_table->table[i].dwIndex, ipaddr_table->table[i].dwAddr);
      NetworkInterface nif;
      nif.index = ipaddr_table->table[i].dwIndex;
      nif.ip4_address_u32 = ipaddr_table->table[i].dwAddr;
      snprintf (nif.label, sizeof (nif.label), "ipv4:%" PRIu32, nif.index);
      assert (network.interfaces.count < network.interfaces.max);
      network.interfaces.list[network.interfaces.count++] = nif;
    }
}

static b8
list_models (void)
{
  WIN32_FIND_DATAA file_entry;
  HANDLE search_handle = FindFirstFileA ("res/models/*.obj", &file_entry);
  assert (search_handle);

  do
    {
      char  *name = file_entry.cFileName;
      size_t name_len = strlen (name);
      char model_name[name_len];
      strcpy (model_name, name);
      model_name[name_len - 4] = 0;
      console_print (model_name);
      console_print ("\n");
    }
  while (FindNextFileA (search_handle, &file_entry));

  FindClose (search_handle);
  return 0;
}

static b8
list_levels (void)
{
  WIN32_FIND_DATAA file_entry;
  HANDLE search_handle = FindFirstFileA ("res/levels/*.txt", &file_entry);
  assert (search_handle);

  do
    {
      char  *name = file_entry.cFileName;
      size_t name_len = strlen (name);
      char level_name[name_len];
      strcpy (level_name, name);
      level_name[name_len - 4] = 0;
      console_print (level_name);
      console_print ("\n");
    }
  while (FindNextFileA (search_handle, &file_entry));

  FindClose (search_handle);
  return 0;
}


static void
load_models (const char *dirpath)
{
  size_t dirpath_len = strlen (dirpath);
  size_t search_path_len = dirpath_len + strlen ("/*.obj");
  char   search_path[search_path_len + 1];
  strcpy (search_path, dirpath);
  strcat (search_path, "/*.obj");

  WIN32_FIND_DATAA file_entry;
  HANDLE search_handle = FindFirstFileA (search_path, &file_entry);
  assert (search_handle);

  assets.models = MALLOC (Model, assets.models_max);

  do
    {
      assert (assets.models_count < assets.models_max);

      char  *name = file_entry.cFileName;
      size_t name_len = strlen (name);

      size_t filepath_len = dirpath_len + 1 + name_len;
      char   filepath[filepath_len + 1];
      strcpy (filepath, dirpath);
      strcat (filepath, "/");
      strcat (filepath, name);

      assets.models[assets.models_count++] = load_model (filepath, name);
    }
  while (FindNextFileA (search_handle, &file_entry));

  FindClose (search_handle);
}
