/* Math
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


union V2 {
  struct {r32 x,y;};
  r32 e[2];
};


union V3 {
  struct {r32 x,y,z;};
  struct {r32 r,g,b;};
  r32 e[3];
};


struct Mat4x4 {
  r32 m[16];
};


static V3
operator- (V3 v)
{
  v.x = -v.x;
  v.y = -v.y;
  v.z = -v.z;
  return v;
}

static V3
operator- (V3 a, V3 b)
{
  a.x -= b.x;
  a.y -= b.y;
  a.z -= b.z;
  return a;
}

static V3 &
operator-= (V3 &a, r64 b)
{
  a.x -= b;
  a.y -= b;
  a.z -= b;
  return a;
}

static V3 &
operator-= (V3 &a, V3 b)
{
  a.x -= b.x;
  a.y -= b.y;
  a.z -= b.z;
  return a;
}


static V3
operator+ (V3 a, V3 b)
{
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  return a;
}

static V3 &
operator+= (V3 &a, r64 b)
{
  a.x += b;
  a.y += b;
  a.z += b;
  return a;
}

static V3 &
operator+= (V3 &a, V3 b)
{
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  return a;
}


static V2
operator/ (V2 v, r64 a)
{
  v.x = v.x / a;
  v.y = v.y / a;
  return v;
}

static V3
operator/ (V3 v, r64 a)
{
  v.x = v.x / a;
  v.y = v.y / a;
  v.z = v.z / a;
  return v;
}


static V3
operator* (V3 v, r64 n)
{
  v.x *= n;
  v.y *= n;
  v.z *= n;
  return v;
}

static V3
operator* (V3 v, V3 v1)
{
  v.x *= v1.x;
  v.y *= v1.y;
  v.z *= v1.z;
  return v;
}

static V3 &
operator*= (V3 &a, r64 b)
{
  a.x *= b;
  a.y *= b;
  a.z *= b;
  return a;
}


static V3
cross (V3 a, V3 b)
{
  return {
    a.y * b.z - b.y * a.z,
    a.z * b.x - b.z * a.x,
    a.x * b.y - b.x * a.y,
  };
}

static r32
dot (V3 a, V3 b)
{
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

static r32
inverse_sqrt (r32 x)
{
  r32 half = x * 0.5f;
  int i = *(int *) &x;
  i = 0x5f3759df - (i >> 1);
  x = *(float *) &i;
  x = x * (1.5f - half * x * x);
  return x;
}

static V3
normalize (V3 v)
{
  return v * inverse_sqrt (dot (v, v));
}

static Mat4x4
operator* (Mat4x4 a, Mat4x4 b)
{
  Mat4x4 r;
  r.m[0]  = a.m[ 0]*b.m[0] + a.m[ 1]*b.m[4] + a.m[ 2]*b.m[ 8] + a.m[ 3]*b.m[12];
  r.m[1]  = a.m[ 0]*b.m[1] + a.m[ 1]*b.m[5] + a.m[ 2]*b.m[ 9] + a.m[ 3]*b.m[13];
  r.m[2]  = a.m[ 0]*b.m[2] + a.m[ 1]*b.m[6] + a.m[ 2]*b.m[10] + a.m[ 3]*b.m[14];
  r.m[3]  = a.m[ 0]*b.m[3] + a.m[ 1]*b.m[7] + a.m[ 2]*b.m[11] + a.m[ 3]*b.m[15];

  r.m[4]  = a.m[ 4]*b.m[0] + a.m[ 5]*b.m[4] + a.m[ 6]*b.m[ 8] + a.m[ 7]*b.m[12];
  r.m[5]  = a.m[ 4]*b.m[1] + a.m[ 5]*b.m[5] + a.m[ 6]*b.m[ 9] + a.m[ 7]*b.m[13];
  r.m[6]  = a.m[ 4]*b.m[2] + a.m[ 5]*b.m[6] + a.m[ 6]*b.m[10] + a.m[ 7]*b.m[14];
  r.m[7]  = a.m[ 4]*b.m[3] + a.m[ 5]*b.m[7] + a.m[ 6]*b.m[11] + a.m[ 7]*b.m[15];

  r.m[8]  = a.m[ 8]*b.m[0] + a.m[ 9]*b.m[4] + a.m[10]*b.m[ 8] + a.m[11]*b.m[12];
  r.m[9]  = a.m[ 8]*b.m[1] + a.m[ 9]*b.m[5] + a.m[10]*b.m[ 9] + a.m[11]*b.m[13];
  r.m[10] = a.m[ 8]*b.m[2] + a.m[ 9]*b.m[6] + a.m[10]*b.m[10] + a.m[11]*b.m[14];
  r.m[11] = a.m[ 8]*b.m[3] + a.m[ 9]*b.m[7] + a.m[10]*b.m[11] + a.m[11]*b.m[15];

  r.m[12] = a.m[12]*b.m[0] + a.m[13]*b.m[4] + a.m[14]*b.m[ 8] + a.m[15]*b.m[12];
  r.m[13] = a.m[12]*b.m[1] + a.m[13]*b.m[5] + a.m[14]*b.m[ 9] + a.m[15]*b.m[13];
  r.m[14] = a.m[12]*b.m[2] + a.m[13]*b.m[6] + a.m[14]*b.m[10] + a.m[15]*b.m[14];
  r.m[15] = a.m[12]*b.m[3] + a.m[13]*b.m[7] + a.m[14]*b.m[11] + a.m[15]*b.m[15];

  return r;
}

static Mat4x4 &
operator*= (Mat4x4 &a, Mat4x4 b)
{
  a = a * b;
  return a;
}


static r32
rand32 (void)
{
  return (r32) rand () / (RAND_MAX + 1ULL);
}
