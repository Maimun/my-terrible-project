/* GNU/Linux Utils
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


static V2
draw_glyphs (String string, V2 pos, r32 *transform)
{
  r32 glyph_transformed_w = assets.font.glyph_w * transform[0];
  r32 glyph_twidth = 1.0f / assets.font.glyph_count;
  r32 line_spacing = assets.font.glyph_h * transform[5];

  transform[3] = pos.x;
  transform[7] = pos.y;

  for (; string.size; --string.size)
    {
      char c = (string.data++)[0];

      if (c == '\n')
        {
          transform[3]  = pos.x;
          transform[7] -= line_spacing;
        }
      else
        {
          u8 glyph_index = c - ' ';
          r32 texcoord_xoffset = glyph_twidth * glyph_index;
          glUniform1f (assets.font_shader.texcoord_xoffset_uni, texcoord_xoffset);
          glUniformMatrix4fv (assets.font_shader.transform_uni, 1, GL_FALSE, transform);
          glDrawArrays (GL_TRIANGLE_STRIP, 0, 4);
          transform[3] += glyph_transformed_w;
        }
    }

  pos.x = transform[3];
  pos.y = transform[7];
  return pos;
}


static V2
draw_text_at_pos (V2 pos, String text)
{
  r32 wx = game_state.font_scaler.x;
  r32 wy = game_state.font_scaler.y;

  r32 transform[16] = {
    wx,  0,  0,  0,
     0, wy,  0,  0,
     0,  0,  1,  0,
     0,  0,  0,  1,
  };

  glUseProgram (assets.font_shader.gl_id);

  glActiveTexture (GL_TEXTURE0);
  glUniform1i (assets.font_shader.texture_uni, 0);
  glEnableVertexAttribArray (assets.font_shader.position_attrib);
  glEnableVertexAttribArray (assets.font_shader.texcoord_attrib);

  glUniform4f (assets.font_shader.color_uni, 0, 0, 0, 1);
  glBindTexture (GL_TEXTURE_2D, assets.font.background.gl_id);
  glBindBuffer (GL_ARRAY_BUFFER, assets.font.background_rect_array_buffer);
  glVertexAttribPointer (assets.font_shader.position_attrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(r32), 0);
  glVertexAttribPointer (assets.font_shader.texcoord_attrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(r32), (void *) (2 * sizeof(r32)));
  draw_glyphs (text, pos, transform);

  glUniform4f (assets.font_shader.color_uni, 1, 1, 1, 1);
  glBindTexture (GL_TEXTURE_2D, assets.font.foreground.gl_id);
  glBindBuffer (GL_ARRAY_BUFFER, assets.font.foreground_rect_array_buffer);
  glVertexAttribPointer (assets.font_shader.position_attrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(r32), 0);
  glVertexAttribPointer (assets.font_shader.texcoord_attrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(r32), (void *) (2 * sizeof(r32)));
  pos = draw_glyphs (text, pos, transform);

  glBindBuffer (GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray (assets.font_shader.position_attrib);
  glDisableVertexAttribArray (assets.font_shader.texcoord_attrib);

  return pos;
}


static V2
draw_left_aligned_valist_text_at_pos (V2 pos, const char *format, va_list format_args)
{
  char   text_buffer[4096];
  String text = {text_buffer, sizeof (text_buffer)};
  String end = string_append_formatted (text, format, format_args);
  text.size -= end.size;

  pos = draw_text_at_pos (pos, text);
  return pos;
}


static V2
draw_right_aligned_text_at_pos (V2 pos, String text)
{
  pos.x -= assets.font.glyph_w * string_get_max_line_len (text) * game_state.font_scaler.x;
  return draw_text_at_pos (pos, text);
}


static V2
draw_right_aligned_valist_text_at_pos (V2 pos, const char *format, va_list format_args)
{
  char   text_buffer[4096];
  String text = {text_buffer, sizeof (text_buffer)};
  String end = string_append_formatted (text, format, format_args);
  text.size -= end.size;

  return draw_right_aligned_text_at_pos (pos, text);
}


static V2
draw_centered_text (s32 x, s32 y, const char *format, ...)  __attribute__ ((__format__ (__printf__, 3, 4)));
static V2
draw_centered_text (s32 x, s32 y, const char *format, ...)
{
  V2 pos = {x * game_state.font_scaler.x,
            y * game_state.font_scaler.y};
  va_list format_args;
  va_start (format_args, format);
  pos = draw_left_aligned_valist_text_at_pos (pos, format, format_args);
  va_end (format_args);
  return pos;
}


static V2
draw_dl_text (s32 x, s32 y, const char *format, ...)  __attribute__ ((__format__ (__printf__, 3, 4)));
static V2
draw_dl_text (s32 x, s32 y, const char *format, ...)
{
  V2 pos = {-1 + x * game_state.font_scaler.x,
            -1 + y * game_state.font_scaler.y};
  va_list format_args;
  va_start (format_args, format);
  pos = draw_left_aligned_valist_text_at_pos (pos, format, format_args);
  va_end (format_args);
  return pos;
}


static V2
draw_dr_text (s32 x, s32 y, const char *format, ...)  __attribute__ ((__format__ (__printf__, 3, 4)));
static V2
draw_dr_text (s32 x, s32 y, const char *format, ...)
{
  V2 pos = { 1 - x * game_state.font_scaler.x,
            -1 + y * game_state.font_scaler.y};

  va_list format_args;
  va_start (format_args, format);
  pos = draw_right_aligned_valist_text_at_pos (pos, format, format_args);
  va_end (format_args);
  return pos;
}


static V2
draw_ul_text (s32 x, s32 y, const char *format, ...)  __attribute__ ((__format__ (__printf__, 3, 4)));
static V2
draw_ul_text (s32 x, s32 y, const char *format, ...)
{
  V2 pos;
  pos.x = -1 + x * game_state.font_scaler.x;
  pos.y =  1 - y * game_state.font_scaler.y - assets.font.glyph_h * game_state.font_scaler.y;

  va_list format_args;
  va_start (format_args, format);
  pos = draw_left_aligned_valist_text_at_pos (pos, format, format_args);
  va_end (format_args);
  return pos;
}


static V2
draw_ur_text (s32 x, s32 y, const char *format, ...)  __attribute__ ((__format__ (__printf__, 3, 4)));
static V2
draw_ur_text (s32 x, s32 y, const char *format, ...)
{
  V2 pos;
  pos.x =  1 - x * game_state.font_scaler.x;
  pos.y =  1 - y * game_state.font_scaler.y - assets.font.glyph_h * game_state.font_scaler.y;

  va_list format_args;
  va_start (format_args, format);
  pos = draw_right_aligned_valist_text_at_pos (pos, format, format_args);
  va_end (format_args);
  return pos;
}


static V2
draw_ur_text (s32 x, s32 y, String text)
{
  V2 pos;
  pos.x =  1 - x * game_state.font_scaler.x;
  pos.y =  1 - y * game_state.font_scaler.y - assets.font.glyph_h * game_state.font_scaler.y;
  return draw_right_aligned_text_at_pos (pos, text);
}
