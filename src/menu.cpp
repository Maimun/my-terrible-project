/* GNU/Linux Utils
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


static b8
is_inside (V2 pos0, V2 pos1, V2 dim1)
{
   V2 half_dim1 = dim1 / 2;
  return (pos0.x > pos1.x - half_dim1.x &&
          pos0.x < pos1.x + half_dim1.x &&
          pos0.y > pos1.y - half_dim1.y &&
          pos0.y < pos1.y + half_dim1.y);
}

static void
draw_rect (s32 x, s32 y, s32 w, s32 h)
{
  w /= 2;
  h /= 2;
  draw_centered_text (x - w, y - h, "+");
  draw_centered_text (x + w, y - h, "+");
  draw_centered_text (x - w, y + h, "+");
  draw_centered_text (x + w, y + h, "+");
}

static void
levels_menu ()
{
  for (u32 i = 0; i < 10; i++)
    {
      draw_rect (-game_state.width / 2 + i * 50, game_state.height - 50, 40, 40);
    }
}

static void
start_menu ()
{
  draw_text_at_pos (game_state.mouse_pos, to_string ("+"));
  draw_rect (0, 0, 50, 20);
  if (is_inside (game_state.mouse_pos, (V2){0, 0}, (V2){50, 20}) && input.mouse_left_button)
    {
      levels_menu ();
    }
}

