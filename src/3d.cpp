/* 3D Game
 *
 * Copyright (C) 2019 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define _USE_MATH_DEFINES
#include <math.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>

#include <stdint.h>
#include <inttypes.h>        // PRIu64 PRId64 PRIx64 PRIu32 PRId32 ...

#ifndef ROOT_PATH
#define ROOT_PATH "."
#endif

#define FONT_FILEPATH         ROOT_PATH "/res/images/font6x10.data"
#define TEXT_VSHADER_FILEPATH ROOT_PATH "/res/shaders/text_vshader.glsl"
#define TEXT_FSHADER_FILEPATH ROOT_PATH "/res/shaders/text_fshader.glsl"
#define VSHADER_FILEPATH      ROOT_PATH "/res/shaders/vshader.glsl"
#define FSHADER_FILEPATH      ROOT_PATH "/res/shaders/fshader.glsl"
#define MODELS_DIRPATH        ROOT_PATH "/res/models"
#define LEVELS_DIRPATH        ROOT_PATH "/res/levels"
#define GAME_INIT_WINDOW_WIDTH  960
#define GAME_INIT_WINDOW_HEIGHT 640
#define CONSOLE_MAX_LINES 31

typedef float  r32;
typedef double r64;

typedef uint8_t b8;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

struct String;
static void console_print (const char *format, ...)  __attribute__ ((__format__ (__printf__, 1, 2)));
static void console_print (String string);

#include "math.cpp"
#include "buffers.cpp"

struct CollisionBox {
  V3 pos;
  V3 dim;
};

struct Model {
  GLuint array_buffer;
  u32 vertices_count;
  String name;

  u8 collision_boxes_count;
  CollisionBox *collision_boxes;
};

struct Object {
  Model *model;
  V3 pos;
  V3 rot;
  V3 scale;
  Object *child;
  V3 rot_anim_speed;
};


enum EditorMode {
  EDITOR_MODE_OFF,
  EDITOR_MODE_ABSOLUTE,
  EDITOR_MODE_RELATIVE,
};


struct Message {
  r32 time;
  String text;
};


struct NetworkInterface {
  u32 index;
  union {
    u8  ip4_address[4];
    u32 ip4_address_u32;
  };
  char label[64];
};


struct NetworkInterfacesList {
  NetworkInterface list[16];
  u32 max = 16;
  u32 count;
};


enum NetworkStatus {
  NETWORK_NONE,
  NETWORK_LISTENING,
  NETWORK_CONNECTING,
  NETWORK_CONNECTED,
  NETWORK_DISCONNECTED,
};


struct Network {
  NetworkStatus status;
  NetworkInterfacesList interfaces;
  Buffer  send_data_buffer;
  Buffer  messages_buffer;
  Buffer  messages_buffer_p;
  String *messages;
  u32     messages_count;
  u32     messages_max;
  u32     bytes_recv;
  u32     bytes_sent;

  u32     invalid_messages_recv_count;
  u32     chats_recv_count;
  u32     positions_recv_count;

} network;


struct GameState {
  b8 keep_running;

  u32 current_level_time;
  u32 level_times[3] = {(u32)-1,(u32)-1,(u32)-1};

  EditorMode editor_mode = EDITOR_MODE_ABSOLUTE;
  V3 editor_cursor_pos = (V3){-1, -1.5, -2};
  V3 editor_cursor_rot;
  V3 editor_cursor_scale = (V3){0.1, 0.1, 0.1};
  V3 editor_cursor_save_pos;

  u32 width;
  u32 height;
  V2 font_scaler;
  SDL_Window *window;

  char    level_name_buffer[128];
  String  level_name;

  u32 frame_logs_y;

  String  console_log;
  String  console_log_buffer;
  u32     console_scroll;
  String  text_input;
  String  text_input_buffer;
  String  chat_input;
  String  chat_input_buffer;
  b8 show_console;
  b8 chat;

  b8 show_collisions;

  Message messages;
  String  messages_buffer;

  V3 camera_pos;
  Mat4x4 view_matrix;
  Mat4x4 proj_matrix;

  u32     objects_max = 512;
  u32     objects_count;
  Object *objects;

  V3  player_save_pos;
  V3  player_pos;
  V3  player_dim;
  V3  player_velocity;
  V3  second_player_pos;
  r32 second_player_hangle;

  V2 mouse_pos;
  s32 sensitivity;
} game_state;


#include "network.cpp"
#include "assets.cpp"


#ifndef OS_WINDOWS
#ifndef OS_GNULINUX
#error ("You should define OS_GNULINUX or OS_WINDOWS.");
#endif
#endif

#ifdef OS_WINDOWS
#ifdef OS_GNULINUX
#error ("You should define OS_GNULINUX or OS_WINDOWS, but not both.");
#endif
#endif

#ifdef OS_WINDOWS
#include "system_windows.cpp"
#endif

#ifdef OS_GNULINUX
#include "system_gnulinux.cpp"
#endif


struct Input {
  b8 show_console;
  b8 toggle_collisions_view;

  r32 player_x;
  r32 player_y;
  b8 mouse_middle_button;
  b8 mouse_left_button;

  b8 forward;
  b8 back;
  b8 left;
  b8 right;
  b8 down;
  b8 up;
  b8 chat;
  b8 respawn;

  b8 next;
  b8 prev;

  b8 p2_left;
  b8 p2_right;
  b8 p2_back;
  b8 p2_forward;
  b8 p2_up;
  b8 p2_down;
  b8 p2_lturn;
  b8 p2_rturn;
  b8 p2_scale_down;
  b8 p2_scale_up;
  b8 x;
  b8 y;
  b8 z;

  b8 add_object;
  b8 del_object;
  b8 copy_object;
  b8 edit_object;
  b8 step_edit;
} input;


static void
console_print (const char *format, ...)
{
  va_list format_args;
  va_start (format_args, format);
  game_state.console_log = string_append_formatted (game_state.console_log, format, format_args);
  va_end (format_args);
}

static void
console_print (String string)
{
  game_state.console_log = string_append_string (game_state.console_log, string);
}


static void
print (String string)
{
  game_state.console_log = string_append_string (game_state.console_log, string);
}


#include "draw_text.cpp"
// #include "menu.cpp"

static void
rescale_game (int width, int height)
{
  game_state.width = width;
  game_state.height = height;
  game_state.font_scaler.x = 4.0f / game_state.width;
  game_state.font_scaler.y = 4.0f / game_state.height;
  glViewport (0, 0, game_state.width, game_state.height);
}


static void
respawn_to_start (void)
{
  game_state.player_pos = {};
  game_state.player_velocity = {};
}

static void
respawn_to_checkpoint (void)
{
  game_state.player_pos = game_state.player_save_pos;
  game_state.player_velocity = {};
}


static void
savepos (void)
{
  game_state.player_save_pos = game_state.player_pos;
}


static void
set_respawn_pos (V3 pos)
{
  game_state.player_save_pos = pos;
}


static void
draw_console (void)
{
  String log;
  log.data = game_state.console_log_buffer.data;
  log.size = game_state.console_log_buffer.size - game_state.console_log.size;
  
  draw_centered_text (0, 0, "%u", game_state.console_scroll);
  u32 lines_count = 0;
  u32 scroll = game_state.console_scroll;
  u32 first_line = CONSOLE_MAX_LINES + scroll;
  
  for (u32 i = log.size; i > 0;)
    {
      --i;
      if (log.data[i] == '\n')
        {
          if (lines_count == first_line)
            {
              log.data += i+1;
              log.size -= i+1;
              break;
            }
          lines_count++;
        }
    }
  
  if (scroll)
    {
      u32 lines_count = 0;
      for (u32 i = 0; i < log.size; i++)
        {
          if (log.data[i] == '\n')
           {
             lines_count++;
             if (lines_count == CONSOLE_MAX_LINES)
               {
                 log.size = i+1;
                 break;
               }
           }
        }
    }
  
  V2 pos;
  pos.x = -1 + 0 * game_state.font_scaler.x;
  pos.y =  1 - 0 * game_state.font_scaler.y - assets.font.glyph_h * game_state.font_scaler.y;

  pos = draw_text_at_pos (pos, log);

  String text_input;
  text_input.data = game_state.text_input_buffer.data;
  text_input.size = game_state.text_input_buffer.size - game_state.text_input.size;

  pos = draw_text_at_pos (pos, to_string ("> "));
  pos = draw_text_at_pos (pos, text_input);

  if (SDL_GetTicks () / 500 % 2)
    {
      pos = draw_text_at_pos (pos, to_string ("_"));
    }
}


static void
draw_chat (void)
{
  // String log;
  // log.data = game_state.console_log_buffer.data;
  // log.size = game_state.console_log_buffer.size - game_state.console_log.size;

  V2 pos;
  pos.x = -1 + 0 * game_state.font_scaler.x;
  pos.y =  1 - 0 * game_state.font_scaler.y - assets.font.glyph_h * game_state.font_scaler.y;

  // pos = draw_text_at_pos (pos, log);

  String chat_input;
  chat_input.data = game_state.chat_input_buffer.data;
  chat_input.size = game_state.chat_input_buffer.size - game_state.chat_input.size;

  pos = draw_text_at_pos (pos, to_string ("# "));
  pos = draw_text_at_pos (pos, chat_input);

  if (SDL_GetTicks () / 500 % 2)
    {
      pos = draw_text_at_pos (pos, to_string ("_"));
    }
}


static void
clear_console (void)
{
  game_state.console_log = game_state.console_log_buffer;
}


static void
clear_console_input (void)
{
  game_state.text_input = game_state.text_input_buffer;
}

static void
clear_chat (void)
{
  //game_state.console_log = game_state.console_log_buffer;
}


static void
clear_chat_input (void)
{
  game_state.chat_input = game_state.chat_input_buffer;
}

static u32
parse_ip4addr (String string)
{
  u64 a = string_get_u64 (&string);
  if (a == 0) return 0;
  if (a > 255) return 0;
  if (string.data[0] != '.') return 0;
  ++string.data;
  --string.size;

  u64 b = string_get_u64 (&string);
  if (b > 255) return 0;
  if (string.data[0] != '.') return 0;
  ++string.data;
  --string.size;

  u64 c = string_get_u64 (&string);
  if (c > 255) return 0;
  if (string.data[0] != '.') return 0;
  ++string.data;
  --string.size;

  u64 d = string_get_u64 (&string);
  if (d == 0) return 0;
  if (d > 255) return 0;
  if (string.size) return 0;

  u32 addr = (a << 24) | (b << 16) | (c << 8) | d;
  return addr;
}


static String
get_word (String *string)
{
  String p = string[0];
  while (p.size && p.data[0] == ' ') {--p.size; ++p.data;}
  String r = p;
  while (p.size && p.data[0] != ' ') {--p.size; ++p.data;}
  r.size -= p.size;

  string[0] = p;

  return r;
}


static void
submit_chat_input (void)
{
  String chat_input;
  chat_input.data = game_state.chat_input_buffer.data;
  chat_input.size = game_state.chat_input_buffer.size - game_state.chat_input.size;

  //console_print ("> ");
  //console_print (text_input);
  //console_print ("\n");

  send_chat (chat_input);
  clear_chat_input ();
}


static void
submit_console_input (void)
{
  String text_input;
  text_input.data = game_state.text_input_buffer.data;
  text_input.size = game_state.text_input_buffer.size - game_state.text_input.size;

  console_print ("> ");
  console_print (text_input);
  console_print ("\n");

  String command = get_word (&text_input);

  if (command.size == 0)
    {
      return;
    }
  else if (command == "help")
    {
      console_print ("Commands:\n"
                     "  help\n"
                     "  clear\n"
                     "  sensitivity\n"
                     "  controls\n"
                     "  respawn\n"
                     "  savepos\n"
                     "  listlevels\n"
                     "  listmodels\n"
                     "  savelevel [LEVEL]\n"
                     "  loadlevel LEVEL\n"
                     "  newlevel [NAME]\n"
                     "  connect IP4ADDRESS\n"
                     "  listen\n"
                     "  editor relative|absolute|off\n"
                     "  seteditpos X Y Z\n"
                     "  seteditrot X Y Z\n"
                     "  seteditscale X Y Z\n"
                     "  undo\n"
                     "  quit\n");
    }
  else if (command == "clear")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: clear doesn't take arguments.\n");
          return;
        }

      clear_console ();
      clear_console_input ();
    }
  else if (command == "respawn")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: respawn doesn't take arguments.\n");
          return;
        }

      respawn_to_checkpoint ();
    }
  else if (command == "controls")
    {
      console_print ("W,A,S,D,SPACE - MOVE\n"
                     "Arrow keys - move editor (the snowman with monkey head)\n"
                     "'.' and ',' - switch model for editor\n"
                     "right enter - up editor, right shift - down editor\n"
                     "'[' and ']' - rotate editor\n"
                     "'-' and '+' - scale down/up editor (hold x y or z to squash it in any way)\n"
                     "m - place model\n"
                     "'~' - console (type help for a list of all the commands)\n"
                     "Hold 'Ctrl' while scaling or rotating the editor to do it in an equal amount\n of space, angle, scale steps - for example: not 17.241 degrees, but 15 degrees)\n"
                     "How to make a level:\n"
                     "Step 1:Make a new text file in res/levels and call it (whatever you want).txt\n"
                     "Step 2:Copy first 4 rows of 1.txt, paste them into the file and go in game.\n Load it with 'loadlevel (the name of your level)' command in console.\n"
                     "Step 3:Start building with the editor(the monkey with snowman body)\n"
                     "DONT FORGET TO SAVE THE LEVEL WITH savelevel COMMAND!!!!\n"
                     "Checkpoints:\nMake sure to always first add the checkpoint flag and then the object beneath it.\n"
                     "FULLSCREEN: Win+ArrowUp(Windows 10)\n"
                     "type clear to clear the console\n");
    }
  else if (command == "savepos")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: savepos doesn't take arguments.\n");
          return;
        }

      savepos ();
    }
  else if (command == "listlevels")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: listlevels doesn't take arguments.\n");
          return;
        }

      list_levels ();
    }
  else if (command == "listmodels")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: listmodels doesn't take arguments.\n");
          return;
        }

      list_models ();
    }
  else if (command == "savelevel")
    {
      String level_name = get_word (&text_input);

      if (level_name.size)
        {
          if (get_word (&text_input).size)
            {
              console_print ("Error: savelevel takes only 1 argument.\n");
              return;
            }
        }
      else
        {
          if (!game_state.level_name.size)
            {
              console_print ("Error: savelevel required level name because there's no current active level.\n");
              return;
            }
          level_name = game_state.level_name;
        }

      if (!save_level (level_name))
        {
          return;
        }

      console_print ("Level saved.\n");
    }

  else if (command == "loadlevel")
    {
      String level_name = get_word (&text_input);
      if (!level_name.size)
        {
          console_print ("Error: loadlevel requires a level name.\n");
          return;
        }
      
      if (get_word (&text_input).size)
        {
          console_print ("Error: loadlevel takes only 1 argument.\n");
          return;
        }
      
      if (!load_level (level_name))
        {
          return;
        }
      
      console_print ("Level loaded.\n");
      respawn_to_start ();
      savepos ();
    }

  else if (command == "newlevel")
    {
      String level_name = get_word (&text_input);
      if (!level_name.size)
        {
          console_print ("Error: newlevel requires a level name.\n");
          return;
        }
      
      if (get_word (&text_input).size)
        {
          console_print ("Error: newlevel takes only 1 argument.\n");
          return;
        }
      
      size_t level_filepath_len = strlen ("res/levels/") + level_name.size + strlen (".txt");
      char level_filepath[level_filepath_len + 1];
      level_filepath[0] = 0;
      strcpy (level_filepath, "res/levels/");
      memcpy (level_filepath + strlen ("res/levels/"), level_name.data, level_name.size);
      level_filepath[strlen ("res/levels/") + level_name.size] = 0;
      strcat (level_filepath, ".txt");
      
      FILE *level_file = fopen (level_filepath, "r");
      if (level_file)
        {
          console_print ("Error: newlevel requires an unique level name.\n");
          fclose (level_file);
          return;
        }
      
      level_file = fopen (level_filepath, "w");
      fprintf (level_file,
               "cube\n"
               "0.000000 -2.000000  0.000000\n"
               "0.000000  0.000000  0.000000\n"
               "0.200000  0.200000  0.200000\n");
      fclose (level_file);
      
      if (!load_level (level_name))
        {
          return;
        }
      
      console_print ("Level created.\n");
      respawn_to_start ();
      savepos ();
    }

  else if (command == "connect")
    {
      String address = get_word (&text_input);
      if (address.size == 0)
        {
          console_print ("Error: connect requires an IP4 address.\n");
          return;
        }

      if (get_word (&text_input).size)
        {
          console_print ("Error: connect takes only 1 argument.\n");
          return;
        }

      if (network.status == NETWORK_NONE || network.status == NETWORK_DISCONNECTED)
        {
          if (ip4_connect (address))
            {
              network.status = NETWORK_CONNECTING;
              start_network_connection_thread ();
            }
          else
            {
              return;
            }
        }
      else
        {
          console_print ("Error: Can connect only when not connected.\n");
        }
    }
  else if (command == "listen")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: listen takes only 0 argument.\n");
          return;
        }

      if (network.status == NETWORK_NONE || network.status == NETWORK_DISCONNECTED)
        {
          ip4_listen ();
          network.status = NETWORK_LISTENING;
          start_network_connection_thread ();
        }
      else
        {
          console_print ("Error: Can listen only when not connected.\n");
        }
    }
  else if (command == "sensitivity")
    {
      String value = get_word (&text_input);

      if (!value.size)
        {
          console_print ("Error: editor requires an argument.\n");
          return;
        }

      if (get_word (&text_input).size)
        {
          console_print ("Error: editor takes only 1 argument.\n");
          return;
        }

      game_state.sensitivity = string_get_r32 (&value);
    }
  else if (command == "editor")
    {
      String value = get_word (&text_input);

      if (!value.size)
        {
          console_print ("Error: editor requires an argument.\n");
          return;
        }

      if (get_word (&text_input).size)
        {
          console_print ("Error: editor takes only 1 argument.\n");
          return;
        }

      if (value == "relative")
        {
          game_state.editor_mode = EDITOR_MODE_RELATIVE;
        }
      else if (value == "absolute")
        {
          game_state.editor_mode = EDITOR_MODE_ABSOLUTE;
        }
      else if (value == "off")
        {
          game_state.editor_mode = EDITOR_MODE_OFF;
        }
      else
        {
          console_print ("Error: Unrecognized argument for editor.\n");
          return;
        }
    }
  else if (command == "seteditpos")
    {
      String x_string = get_word (&text_input);
      String y_string = get_word (&text_input);
      String z_string = get_word (&text_input);

      if (z_string.size == 0 || get_word (&text_input).size)
        {
          console_print ("Error: seteditpos takes 3 arguments.\n");
          return;
        }

      r32 x = string_get_r32 (&x_string);
      r32 y = string_get_r32 (&y_string);
      r32 z = string_get_r32 (&z_string);

      if (x_string.size) {console_print ("Error: Invalid X value.\n"); return;}
      if (y_string.size) {console_print ("Error: Invalid Y value.\n"); return;}
      if (z_string.size) {console_print ("Error: Invalid Z value.\n"); return;}

      game_state.editor_cursor_pos = {x, y, z};
    }
  else if (command == "seteditrot")
    {
      String x_string = get_word (&text_input);
      String y_string = get_word (&text_input);
      String z_string = get_word (&text_input);

      if (z_string.size == 0 || get_word (&text_input).size)
        {
          console_print ("Error: seteditrot takes 3 arguments.\n");
          return;
        }

      r32 x = string_get_r32 (&x_string);
      r32 y = string_get_r32 (&y_string);
      r32 z = string_get_r32 (&z_string);

      if (x_string.size) {console_print ("Error: Invalid X value.\n"); return;}
      if (y_string.size) {console_print ("Error: Invalid Y value.\n"); return;}
      if (z_string.size) {console_print ("Error: Invalid Z value.\n"); return;}

      game_state.editor_cursor_rot = {x, y, z};
    }
  else if (command == "seteditscale")
    {
      String x_string = get_word (&text_input);
      String y_string = get_word (&text_input);
      String z_string = get_word (&text_input);

      if (z_string.size == 0 || get_word (&text_input).size)
        {
          console_print ("Error: seteditscale takes 3 arguments.\n");
          return;
        }

      r32 x = string_get_r32 (&x_string);
      r32 y = string_get_r32 (&y_string);
      r32 z = string_get_r32 (&z_string);

      if (x_string.size) {console_print ("Error: Invalid X value.\n"); return;}
      if (y_string.size) {console_print ("Error: Invalid Y value.\n"); return;}
      if (z_string.size) {console_print ("Error: Invalid Z value.\n"); return;}

      game_state.editor_cursor_scale = {x, y, z};
    }
  else if (command == "undo")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: undo doesn't take arguments.\n");
          return;
        }

      if (game_state.objects_count)
        {
          --game_state.objects_count;
        }
      else
        {
          console_print ("Error: No more objects to remove.\n");
          return;
        }
    }
  else if (command == "quit")
    {
      if (get_word (&text_input).size)
        {
          console_print ("Error: quit doesn't take arguments.\n");
          return;
        }

      game_state.keep_running = 0;
    }
  else
    {
      console_print ("Error: Unknown command. Type 'help' for help\n");
      return;
    }

  clear_console_input ();
}


static void
delete_console_char (void)
{
  if (game_state.text_input.size < game_state.text_input_buffer.size)
    {
      --game_state.text_input.data;
      ++game_state.text_input.size;
    }
}

static void
delete_chat_char (void)
{
  if (game_state.chat_input.size < game_state.chat_input_buffer.size)
    {
      --game_state.chat_input.data;
      ++game_state.chat_input.size;
    }
}


static int
is_inside (V3 pos0, V3 pos1, V3 dim1)
{
  V3 half_dim1 = dim1 / 2;
  return (pos0.x > pos1.x - half_dim1.x &&
          pos0.x < pos1.x + half_dim1.x &&
          pos0.y > pos1.y - half_dim1.y &&
          pos0.y < pos1.y + half_dim1.y &&
          pos0.z > pos1.z - half_dim1.z &&
          pos0.z < pos1.z + half_dim1.z);
}

static void
set_mouse_pos (int x, int y)
{
  game_state.mouse_pos.x = x;
  game_state.mouse_pos.y = y;
}

static void
poll_events (void)
{
  for (SDL_Event event; SDL_PollEvent (&event);)
    {
      switch (event.type)
        {
        case SDL_KEYDOWN:
        case SDL_KEYUP:
          {
            if (game_state.show_console)
              {
                if (event.type == SDL_KEYDOWN)
                  {
                    switch (event.key.keysym.sym)
                      {
                      case SDLK_ESCAPE: game_state.show_console = 0; break;
                      case SDLK_RETURN: submit_console_input (); break;
                      case SDLK_BACKSPACE: delete_console_char (); break;
                      case SDLK_UP: game_state.console_scroll++; break;
                      case SDLK_DOWN:if (game_state.console_scroll) game_state.console_scroll--; break;
                      }

                    if (event.key.keysym.mod & KMOD_CTRL)
                      {
                        switch (event.key.keysym.sym)
                          {
                          case SDLK_l: clear_console (); break;
                          case SDLK_k: clear_console_input (); break;
                          }
                      }
                  }
                break;
              }
            else if (game_state.chat)
              {
                if (event.type == SDL_KEYDOWN)
                  {
                    switch (event.key.keysym.sym)
                      {
                      case SDLK_ESCAPE: game_state.chat = 0; break;
                      case SDLK_RETURN: submit_chat_input (); break;
                      case SDLK_BACKSPACE: delete_chat_char (); break;
                      }

                    if (event.key.keysym.mod & KMOD_CTRL)
                      {
                        switch (event.key.keysym.sym)
                          {
                          case SDLK_l: clear_chat (); break;
                          case SDLK_k: clear_chat_input (); break;
                          }
                      }
                  }
                break;
              }

            b8 key_state = event.type == SDL_KEYDOWN;
            
            switch (event.key.keysym.sym)
              {
              case SDLK_LCTRL:  input.step_edit  = key_state; break;
              case SDLK_m:      input.add_object = key_state; break;
              case SDLK_DELETE: input.del_object = key_state; break;
              case SDLK_c: if (event.key.keysym.mod & KMOD_CTRL) input.copy_object = key_state; break;
              case SDLK_a:      input.left       = key_state; break;
              case SDLK_d:      input.right      = key_state; break;
              case SDLK_s:      input.back       = key_state; break;
              case SDLK_w:      input.forward    = key_state; break;
              case SDLK_t:      input.chat       = key_state; break;
              case SDLK_SPACE:  input.up         = key_state; break;
              case SDLK_LSHIFT: input.down       = key_state; break;
              case SDLK_r:      input.respawn    = key_state; break;
              case SDLK_COMMA:  input.prev       = key_state; break;
              case SDLK_PERIOD: input.next       = key_state; break;

              case SDLK_BACKQUOTE: input.show_console = key_state; break;
              case SDLK_o: input.toggle_collisions_view = key_state; break;

              case SDLK_LEFT:      input.p2_left    = key_state; break;
              case SDLK_RIGHT:     input.p2_right   = key_state; break;
              case SDLK_DOWN:      input.p2_back    = key_state; break;
              case SDLK_UP:        input.p2_forward = key_state; break;
              case SDLK_RIGHTBRACKET:
              case SDLK_KP_0:      input.p2_rturn   = key_state; break;
              case SDLK_LEFTBRACKET:
              case SDLK_KP_1:      input.p2_lturn   = key_state; break;
              case SDLK_RSHIFT:    input.p2_down    = key_state; break;
              case SDLK_RETURN:    input.p2_up      = key_state; break;
              case SDLK_MINUS:     input.p2_scale_down = key_state; break;
              case SDLK_EQUALS:    input.p2_scale_up   = key_state; break;
              case SDLK_x:
                if (event.key.keysym.mod & KMOD_CTRL)
                  input.edit_object = key_state;
                else
                  input.x = key_state;
                break;
              case SDLK_y:         input.y          = key_state; break;
              case SDLK_z:         input.z          = key_state; break;
              }
          } break;

        case SDL_MOUSEMOTION:
          {
            set_mouse_pos (event.motion.x, event.motion.y);
          } break;

        case SDL_MOUSEBUTTONDOWN:
          {
            switch (event.button.button)
              {
              case 1: input.mouse_left_button = 1;
              case 2: input.mouse_middle_button = 1;
              }
          } break;

        case SDL_MOUSEBUTTONUP:
          {
            switch (event.button.button)
              {
              case 1: input.mouse_left_button = 0;
              case 2: input.mouse_middle_button = 0;
              }
          } break;

        case SDL_TEXTINPUT:
          {
            if (game_state.show_console)
              {
                game_state.text_input = string_append_cstr (game_state.text_input, event.text.text);
              }
            else if (game_state.chat)
              {
                game_state.chat_input = string_append_cstr (game_state.chat_input, event.text.text);
              }
          } break;

        case SDL_WINDOWEVENT:
          {
            switch (event.window.event)
              {
              case SDL_WINDOWEVENT_SIZE_CHANGED:
                int width = event.window.data1;
                int height = event.window.data2;
                rescale_game (width, height);
                break;
              }
          } break;

        case SDL_QUIT:
          {
            game_state.keep_running = 0;
            break;
          }
        }
    }

  int x, y;
  SDL_GetMouseState (&x, &y);
  input.player_x = ((int) game_state.width  / 2 - x);
  input.player_y = ((int) game_state.height / 2 - y);
  SDL_WarpMouseInWindow (game_state.window, game_state.width / 2, game_state.height / 2);
}


static void
draw_load_screen (r32 dt, GLint transform_uniform, GLint color_uniform)
{
  static r32 angle = 0;
  angle -= dt;

  {
    r32 c = cosf (angle);
    r32 s = sinf (angle)* 0.5;
    r32 transform[16] = {
      +c, s, 0, 0,
      -s, c, 0, 0,
      +0, 0, 1, 0,
      +0, 0, 0, 1,
    };

    glUniform4f (color_uniform, 0.5, 0.5, 0.5, 1);
    glUniformMatrix4fv (transform_uniform, 1, GL_FALSE, transform);
    glDrawArrays (GL_TRIANGLE_STRIP, 0, 4);
  }

  {
    r32 c = cosf (angle) * 0.5;
    r32 s = sinf (angle) * 0.15;
    r32 transform[16] = {
      +c, s, 0, 0,
      -s, c, 0, 0,
      +0, 0, 1, 0,
      +0, 0, 0, 1,
    };

    glUniform4f (color_uniform, 1, 1, 1, 1);
    glUniformMatrix4fv (transform_uniform, 1, GL_FALSE, transform);
    glDrawArrays (GL_TRIANGLE_STRIP, 0, 4);
  }
}


static void
draw_model (Model model, Mat4x4 model_matrix)
{
  Mat4x4 mvp_matrix = game_state.proj_matrix * game_state.view_matrix * model_matrix;

  glUseProgram (assets.main_shader.gl_id);
  glUniformMatrix4fv (assets.main_shader.transform_uni, 1, GL_FALSE, mvp_matrix.m);
  glUniform4f (assets.main_shader.color_uni, 1, 0, 0, 1);

  glEnableVertexAttribArray (assets.main_shader.position_attrib);
  glEnableVertexAttribArray (assets.main_shader.normal_attrib);
  glEnableVertexAttribArray (assets.main_shader.color_attrib);
  glBindBuffer (GL_ARRAY_BUFFER, model.array_buffer);
  glVertexAttribPointer (assets.main_shader.position_attrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(r32), 0);
  glVertexAttribPointer (assets.main_shader.normal_attrib,   3, GL_FLOAT, GL_FALSE, 9 * sizeof(r32), (void *) (3 * sizeof(r32)));
  glVertexAttribPointer (assets.main_shader.color_attrib,    3, GL_FLOAT, GL_FALSE, 9 * sizeof(r32), (void *) (6 * sizeof(r32)));

  glDrawArrays (GL_TRIANGLES, 0, model.vertices_count);

  glBindBuffer (GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray (assets.main_shader.position_attrib);
  glDisableVertexAttribArray (assets.main_shader.normal_attrib);
  glDisableVertexAttribArray (assets.main_shader.color_attrib);
}


static void
draw_object (Object object, Mat4x4 transform)
{
  Mat4x4 pos_scale = {
    object.scale.x, 0, 0, object.pos.x,
    0, object.scale.y, 0, object.pos.y,
    0, 0, object.scale.z, object.pos.z,
    0, 0, 0, 1,
  };

  r32 xc = cosf (object.rot.x * M_PI);
  r32 xs = sinf (object.rot.x * M_PI);
  Mat4x4 x_rotate = {
    +1.,  0.,  0.,  0.,
    +0.,  xc,  xs,  0.,
    +0., -xs,  xc,  0.,
    +0.,  0.,  0.,  1.,
  };

  r32 yc = cosf (object.rot.y * M_PI);
  r32 ys = sinf (object.rot.y * M_PI);
  Mat4x4 y_rotate = {
    +yc,  0.,  ys,  0.,
    +0.,  1.,  0.,  0.,
    -ys,  0.,  yc,  0.,
    +0.,  0.,  0.,  1.,
  };

  r32 zc = cosf (object.rot.z * M_PI);
  r32 zs = sinf (object.rot.z * M_PI);
  Mat4x4 z_rotate = {
    +zc,  zs,  0.,  0.,
    -zs,  zc,  0.,  0.,
    +0.,  0.,  1.,  0.,
    +0.,  0.,  0.,  1.,
  };

  transform *= pos_scale * x_rotate * y_rotate * z_rotate;

  draw_model (*object.model, transform);
  if (object.child)
    {
      draw_object (object.child[0], transform);
    }
}


static void
draw_object (Object object)
{
  Mat4x4 transform = {
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1,
  };
  draw_object (object, transform);
}


static void
draw_network_interfaces (void)
{
  u32 y = game_state.frame_logs_y;

  for (u32 i = 0; i < network.interfaces.count; ++i)
    {
      NetworkInterface nif = network.interfaces.list[i];
      if (nif.ip4_address_u32 != 0x0100007f)
        {
          draw_ur_text (0, y,
                        "%s: %u.%u.%u.%u\n",
                        nif.label,
                        nif.ip4_address[0],
                        nif.ip4_address[1],
                        nif.ip4_address[2],
                        nif.ip4_address[3]);
          y += assets.font.glyph_h;
        }
    }

  if (y == 0)
    {
      draw_ur_text (0, y, "no network");
      y += assets.font.glyph_h;
    }

  switch (network.status)
    {
    case NETWORK_NONE:         draw_ur_text (0, y, "No connection"); break;
    case NETWORK_LISTENING:    draw_ur_text (0, y, "Listening..."); break;
    case NETWORK_CONNECTING:   draw_ur_text (0, y, "Connecting to %s:%" PRIu16 " ...", inet_ntoa (remote_addr.sin_addr), ntohs (remote_addr.sin_port)); break;
    case NETWORK_CONNECTED:    draw_ur_text (0, y, "Connected to %s:%" PRIu16,         inet_ntoa (remote_addr.sin_addr), ntohs (remote_addr.sin_port)); break;
    case NETWORK_DISCONNECTED: draw_ur_text (0, y, "Disconnected"); break;
    }
  y += assets.font.glyph_h;

  if (network.status == NETWORK_CONNECTED || network.status == NETWORK_CONNECTING)
    {
      draw_ur_text (0, y, "R:%" PRIu32 " S:%" PRIu32, network.bytes_recv, network.bytes_sent);
      y += assets.font.glyph_h;
      draw_ur_text (0, y, "CHAT:%" PRIu32 " POS:%" PRIu32 " INV:%" PRIu32, network.chats_recv_count, network.positions_recv_count, network.invalid_messages_recv_count);
      y += assets.font.glyph_h;
    }

  game_state.frame_logs_y += y;
}


static void
draw_network_messages (void)
{
  u32 y = game_state.frame_logs_y;

  for (u32 message_index = 0; message_index < network.messages_count; ++message_index)
    {
      String message = network.messages[message_index];
      draw_ur_text (0, y, message);
      y += assets.font.glyph_h;
    }

  game_state.frame_logs_y += y;
}



static void
draw_level_time (u32 time)
{
  draw_ur_text (0, game_state.frame_logs_y,
                "%u.%u",
                time / 1000,
                time % 1000);
  game_state.frame_logs_y += assets.font.glyph_h;
}


static void
change_editor_r32 (b8 *key, r32 *var, r32 val)
{
  if (input.step_edit)
    {
      *key = 0;
      *var += val < 0 ? -0.1 : 0.1;
      *var *= 10;
      *var = roundf (*var);
      *var *= 0.1;
    }
  else
    {
      *var += val;
    }
}

static void
change_editor_V3 (b8 *key, V3 *var, r32 val)
{
  if (input.step_edit)
    {
      *key = 0;
      *var += val < 0 ? -0.1 : 0.1;
      *var *= 10;
      var->x = roundf (var->x);
      var->y = roundf (var->y);
      var->z = roundf (var->z);
      *var *= 0.1;
    }
  else
    {
      *var += val;
    }
}


int
main (int argc, char **argv)
{
  String start_level = to_string ("start");

  if (argc  > 2) fprintf (stderr, "Error: Too many arguments\n");
  if (argc == 2) start_level = to_string (argv[1]);

  game_state.console_log_buffer.size = 8192;
  game_state.console_log_buffer.data = MALLOC (char, game_state.console_log_buffer.size);
  game_state.console_log = game_state.console_log_buffer;

  game_state.text_input_buffer.size = 8192;
  game_state.text_input_buffer.data = MALLOC (char, game_state.text_input_buffer.size);
  game_state.text_input = game_state.text_input_buffer;

  game_state.chat_input_buffer.size = 8192;
  game_state.chat_input_buffer.data = MALLOC (char, game_state.chat_input_buffer.size);
  game_state.chat_input = game_state.chat_input_buffer;


  network.send_data_buffer = alloc (4096);
  network.messages_buffer = alloc (32768);
  network.messages_buffer_p = network.messages_buffer;
  network.messages_max = 1024;
  network.messages_count = 0;
  network.messages = MALLOC (String, network.messages_max);
  start_networking ();


  game_state.width = GAME_INIT_WINDOW_WIDTH;
  game_state.height = GAME_INIT_WINDOW_HEIGHT;
  SDL_Init (SDL_INIT_VIDEO);
  SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 1);
  game_state.window = SDL_CreateWindow ("3D Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GAME_INIT_WINDOW_WIDTH, GAME_INIT_WINDOW_HEIGHT, SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
  assert (game_state.window);
  rescale_game (GAME_INIT_WINDOW_WIDTH, GAME_INIT_WINDOW_HEIGHT);
  SDL_WarpMouseInWindow (game_state.window, game_state.width / 2, game_state.height / 2);
  poll_events ();

  SDL_GLContext glcontext = SDL_GL_CreateContext (game_state.window);
  assert (glcontext);
  glewExperimental = GL_TRUE;
  glewInit ();

  SDL_ShowCursor (SDL_DISABLE);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glLineWidth (2);

  assets.font        = load_font (FONT_FILEPATH, 6, 10, 95);
  assets.font_shader = load_shader_program (TEXT_VSHADER_FILEPATH, TEXT_FSHADER_FILEPATH);
  assets.main_shader = load_shader_program (VSHADER_FILEPATH, FSHADER_FILEPATH);
  assets.missing_model = load_model ("res/models/missing_model.obj", "missing_model.obj");
  load_models (MODELS_DIRPATH);
  load_level (start_level);

  r32 player_vangle = 0;
  r32 player_hangle = M_PI;
  game_state.sensitivity = 80;

  game_state.player_dim = {0.2, 1.0, 0.2};
  Object player_object = {};
  player_object.scale = {0.1,0.1,0.1};
  player_object.model = get_model_by_name ("cube");  // Why are we mesh?

  Object player2_object = {};
  player2_object.scale = {0.333, 0.333, 0.333};
  // player2_object.model = get_model_by_name ("player");
  player2_object.model = get_model_by_name ("cowman");

  Model *closed_hand_model = get_model_by_name ("hand-closed");
  Model *opened_hand_model = get_model_by_name ("hand");
  Model *hand_with_gun_model = get_model_by_name ("hand_with_gun");
  Object player_hand_object = {};
  player_hand_object.pos = {0,0,0};
  player_hand_object.rot = {0,0,0};
  player_hand_object.scale = {0.5,0.5,0.5};
  player_hand_object.model = opened_hand_model;
  player_object.child = &player_hand_object;
  b8 player_has_gun = 0;

  Object p2_object = {};
  p2_object.pos = {0.0, 0.0, 0.0};
  p2_object.scale = {0.1, 0.1, 0.1};
  p2_object.model = get_model_by_name ("snowmonkey");

  Uint32 last_time = SDL_GetTicks ();
  for (game_state.keep_running = 1; game_state.keep_running; )
    {
      Uint32 current_time = SDL_GetTicks ();
      Uint32 frame_time = current_time - last_time;
      game_state.current_level_time += frame_time;
      r64 dt = frame_time / 1000.0;
      last_time = current_time;

      game_state.frame_logs_y = 0;
      poll_events ();

      // glClearColor (0.2, 0.3, 1, 1.0);
      // glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // glDisable (GL_DEPTH_TEST);
      // glDisable (GL_CULL_FACE);
      // glEnable (GL_BLEND);

      // start_menu ();

      // SDL_GL_SwapWindow (game_state.window);
      // continue;

      if (input.show_console)
        {
          input.show_console = 0;
          game_state.show_console = 1;
        }
      if (input.chat)
        {
          input.chat = 0;
          game_state.chat = 1;
        }
      if (input.toggle_collisions_view)
        {
          input.toggle_collisions_view = 0;
          game_state.show_collisions = !game_state.show_collisions;
        }

      for (u32 i = 0; i < game_state.objects_count; i++)
        {
          game_state.objects[i].rot += game_state.objects[i].rot_anim_speed * M_PI * dt;
        }

      player_hangle += input.player_x * game_state.sensitivity / 200.0 * dt;
      player_vangle += input.player_y * game_state.sensitivity / 200.0 * dt;
      if      (player_vangle >  M_PI / 2) player_vangle =  M_PI / 2;
      else if (player_vangle < -M_PI / 2) player_vangle = -M_PI / 2;

      V3 player_right;
      player_right.x = sinf (player_hangle - M_PI / 2);
      player_right.y = 0;
      player_right.z = cosf (player_hangle - M_PI / 2);

      V3 player_forward;
      player_forward.x = sinf (player_hangle);
      player_forward.y = 0;
      player_forward.z = cosf (player_hangle);

      V3 player_direction;
      player_direction.x = cosf (player_vangle) * sinf (player_hangle);
      player_direction.y = sinf (player_vangle);
      player_direction.z = cosf (player_vangle) * cosf (player_hangle);

      V3 player_previous_pos = game_state.player_pos;

      game_state.player_velocity.y -= 5 * dt;
      if (game_state.player_velocity.y < -5) game_state.player_velocity.y = -5;
      game_state.player_pos.y += game_state.player_velocity.y * dt;

      // Check collision for falling down:
      b8 on_ground = 0;
      if (game_state.show_collisions) printf ("----------\n");
      for (u32 i = 0; i < game_state.objects_count; i++)
        {
          Object o = game_state.objects[i];
          Model *model = o.model;
          if (game_state.show_collisions)
            {
              printf ("collisions: %u\n", model->collision_boxes_count);
              fflush (stdout);
            }

          for (u32 j = 0; j < model->collision_boxes_count; j++)
            {
              CollisionBox c = model->collision_boxes[j];
              V3 pos = c.pos * o.scale + o.pos;
              V3 dim = c.dim * o.scale;

              if (game_state.show_collisions)
                {
                  printf ("pos: %f, %f, %f\n", pos.x, pos.y, pos.z);
                  printf ("dim: %f, %f, %f\n", dim.x, dim.y, dim.z);
                  printf ("plp: %f, %f, %f\n", game_state.player_pos.x,
                                               game_state.player_pos.y,
                                               game_state.player_pos.z);
                  fflush (stdout);
                }

              if (is_inside (game_state.player_pos, pos, dim + game_state.player_dim))
                {
                  if (o.model->name == "trampoline")
                    {
                      game_state.player_pos = player_previous_pos;
                      game_state.player_velocity.y = 5;
                    }
                  else if (o.model->name == "gun")
                    {
                      player_has_gun = 1;
                      game_state.objects[i--] = game_state.objects[--game_state.objects_count];
                      break;
                    }
                  else if (o.model->name == "checkpoint")
                    {
                      on_ground = 1;
                      game_state.player_pos = player_previous_pos;
                      set_respawn_pos (o.pos + (V3) {0, o.scale.y + 1, 0});
                    }
                  else if (o.model->name == "lava")
                    {
                      respawn_to_checkpoint ();
                    }
                  else if (o.model->name == "finish")
                    {
                      respawn_to_start ();
                      savepos ();
                      if      (game_state.current_level_time < game_state.level_times[0])
                        {
                          game_state.level_times[2] = game_state.level_times[1];
                          game_state.level_times[1] = game_state.level_times[0];
                          game_state.level_times[0] = game_state.current_level_time;
                        }
                      else if (game_state.current_level_time < game_state.level_times[1])
                        {
                          game_state.level_times[2] = game_state.level_times[1];
                          game_state.level_times[1] = game_state.current_level_time;
                        }
                      else if (game_state.current_level_time < game_state.level_times[2])
                        {
                          game_state.level_times[2] = game_state.current_level_time;
                        }
                      game_state.current_level_time = 0;
                    }
                  else
                    {
                      on_ground = 1;
                      game_state.player_pos = player_previous_pos;
                    }
                  break;
                }
            }
        }
      if (on_ground) game_state.player_velocity.y = 0;

      player_previous_pos = game_state.player_pos;

      if (input.respawn) respawn_to_checkpoint ();
      if (input.left)    game_state.player_pos -= player_right * dt * 1.5;
      if (input.right)   game_state.player_pos += player_right * dt * 1.5;
      if (input.back)    game_state.player_pos -= player_forward * dt * 1.5;
      if (input.forward) game_state.player_pos += player_forward * dt * 1.5;
      if (input.up && on_ground) game_state.player_velocity.y = 3;

      // Check collision for moving around:
      for (u32 i = 0; i < game_state.objects_count; i++)
        {
          Object o = game_state.objects[i];
          Model *model = o.model;
          for (u32 j = 0; j < model->collision_boxes_count; j++)
            {
              CollisionBox c = model->collision_boxes[j];
              V3 pos = o.pos + c.pos;
              V3 dim = o.scale * c.dim;

              if (is_inside (game_state.player_pos, pos, dim + game_state.player_dim))
                {
                  r32 top = pos.y + dim.y / 2 + game_state.player_dim.y / 2;

                  if (on_ground && game_state.player_pos.y >= top - game_state.player_dim.y)
                    {
                      game_state.player_pos.y = top;
                    }
                  else
                    {
                      game_state.player_pos = player_previous_pos;
                    }
                }
            }
        }

      if (game_state.player_pos.y < -10) respawn_to_checkpoint ();

      player_object.pos = game_state.player_pos;
      player_object.rot.y = player_hangle / M_PI;
      player_hand_object.rot.x = player_vangle / M_PI;

      if (player_has_gun)
        {
          player_hand_object.model = hand_with_gun_model;
        }
      else
        {
          if (input.mouse_left_button)
            {
              player_hand_object.model = closed_hand_model;
            }
          else
            {
              player_hand_object.model = opened_hand_model;
            }
        }

      if (input.p2_left)     {input.p2_left     = 0; game_state.editor_cursor_pos.x -= game_state.editor_cursor_scale.x;}
      if (input.p2_right)    {input.p2_right    = 0; game_state.editor_cursor_pos.x += game_state.editor_cursor_scale.x;}
      if (input.p2_down)     {input.p2_down     = 0; game_state.editor_cursor_pos.y -= game_state.editor_cursor_scale.y;}
      if (input.p2_up)       {input.p2_up       = 0; game_state.editor_cursor_pos.y += game_state.editor_cursor_scale.y;}
      if (input.p2_forward)  {input.p2_forward  = 0; game_state.editor_cursor_pos.z -= game_state.editor_cursor_scale.z;}
      if (input.p2_back)     {input.p2_back     = 0; game_state.editor_cursor_pos.z += game_state.editor_cursor_scale.z;}

      if (input.p2_rturn)   change_editor_r32 (&input.p2_rturn,   &game_state.editor_cursor_rot.y, -dt);
      if (input.p2_lturn)   change_editor_r32 (&input.p2_lturn,   &game_state.editor_cursor_rot.y,  dt);
      if (input.p2_scale_down)
        {
          if      (input.x) change_editor_r32 (&input.p2_scale_down, &game_state.editor_cursor_scale.x, dt * -0.1);
          else if (input.y) change_editor_r32 (&input.p2_scale_down, &game_state.editor_cursor_scale.y, dt * -0.1);
          else if (input.z) change_editor_r32 (&input.p2_scale_down, &game_state.editor_cursor_scale.z, dt * -0.1);
          else              change_editor_V3  (&input.p2_scale_down, &game_state.editor_cursor_scale,   dt * -0.1);
        }
      if (input.p2_scale_up)
        {
          if      (input.x) change_editor_r32 (&input.p2_scale_up, &game_state.editor_cursor_scale.x,   dt * 0.1);
          else if (input.y) change_editor_r32 (&input.p2_scale_up, &game_state.editor_cursor_scale.y,   dt * 0.1);
          else if (input.z) change_editor_r32 (&input.p2_scale_up, &game_state.editor_cursor_scale.z,   dt * 0.1);
          else              change_editor_V3  (&input.p2_scale_up, &game_state.editor_cursor_scale,     dt * 0.1);
        }

      static u32 current_model = 0;

      if (input.next)
        {
          input.next = 0;
          if (++current_model == assets.models_count) current_model = 0;
          p2_object.model = assets.models + current_model;
        }

      if (input.prev)
        {
          input.prev = 0;
          if (current_model == 0) current_model = assets.models_count - 1;
          else --current_model;
          p2_object.model = assets.models + current_model;
        }

      if (input.add_object)
        {
          input.add_object = 0;
          if (game_state.objects_count < game_state.objects_max)
            {
              Object o = p2_object;
              game_state.objects[game_state.objects_count++] = o;
            }
          else
            {
              // TODO: Make room for more objects when needed.
              fprintf (stderr, "Error: Too many objects!\n");
            }
        }

      if (input.del_object)
        {
          input.del_object = 0;
          for (u32 i = 0; i < game_state.objects_count; i++)
           {
             Object o = game_state.objects[i];
             if (is_inside (p2_object.pos, o.pos, o.scale * 2 + p2_object.scale * 2))
               {
                 game_state.objects[i] = game_state.objects[--game_state.objects_count];
                 break;
               }
           }
        }

      if (input.copy_object)
        {
          input.copy_object = 0;
          for (u32 i = 0; i < game_state.objects_count; i++)
           {
             Object o = game_state.objects[i];
             if (is_inside (p2_object.pos, o.pos, o.scale * 2 + p2_object.scale * 2))
               {
                 p2_object = o;
                 game_state.editor_cursor_pos = o.pos;
                 game_state.editor_cursor_scale = o.scale;
                 game_state.editor_cursor_rot = o.rot;
                 break;
               }
           }
        }

      if (input.edit_object)
        {
          input.edit_object = 0;
          for (u32 i = 0; i < game_state.objects_count; i++)
           {
             Object o = game_state.objects[i];
             if (is_inside (p2_object.pos, o.pos, o.scale * 2 + p2_object.scale * 2))
               {
                 p2_object = o;
                 game_state.editor_cursor_pos = o.pos;
                 game_state.editor_cursor_scale = o.scale;
                 game_state.editor_cursor_rot = o.rot;
                 
                 game_state.objects[i] = game_state.objects[--game_state.objects_count];
                 break;
               }
           }
        }

      if (network.status == NETWORK_CONNECTED || network.status == NETWORK_CONNECTING)
        {
          send_pos (game_state.player_pos, player_hangle);
        }

      player2_object.pos = game_state.second_player_pos;
      player2_object.pos.y -= player2_object.scale.y * 0.5;
      player2_object.rot.y = game_state.second_player_hangle / M_PI;

      glClearColor (0.2, 0.3, 1, 1.0);
      glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      {
        r32 fovy = 45;
        r32 aspect = (r32) game_state.width / game_state.height;
        r32 tan_half_fovy = tanf (fovy / 2.0f);
        r32 znear = 0.01f;
        r32 zfar = 100.0f;

        r32 af = 1 / (aspect * tan_half_fovy);
        r32 f  = 1 / tan_half_fovy;
        r32 z  = -(    zfar + znear) / (zfar - znear);
        r32 zo = -(2 * zfar * znear) / (zfar - znear);
        game_state.proj_matrix = {
          af,  0,  0,  0,
          +0,  f,  0,  0,
          +0,  0,  z, zo,
          +0,  0, -1,  0,
        };
      }

      {
        V3 up = cross (player_right, player_direction);
        V3 center = game_state.player_pos + player_direction;
        V3 f = normalize (center - game_state.player_pos);
        V3 s = normalize (cross (f, up));
        V3 u = cross (s, f);

        game_state.view_matrix = {
          +s.x,  s.y,  s.z, -dot (s, game_state.player_pos),
          +u.x,  u.y,  u.z, -dot (u, game_state.player_pos),
          -f.x, -f.y, -f.z,  dot (f, game_state.player_pos),
          +0.0,  0.0,  0.0,  1.0,
        };
      }

      glEnable (GL_DEPTH_TEST);
      glEnable (GL_CULL_FACE);
      // glFrontFace (GL_CW);
      glDisable (GL_BLEND);

      switch (game_state.editor_mode)
        {
        case EDITOR_MODE_ABSOLUTE:
          p2_object.rot   = game_state.editor_cursor_rot;
          p2_object.pos   = game_state.editor_cursor_pos;
          p2_object.scale = game_state.editor_cursor_scale;
          draw_object (p2_object);
          break;
        case EDITOR_MODE_RELATIVE:
          p2_object.rot   = game_state.editor_cursor_rot;
          p2_object.pos   = game_state.editor_cursor_pos + game_state.player_pos;
          p2_object.scale = game_state.editor_cursor_scale;
          draw_object (p2_object);
          break;
        case EDITOR_MODE_OFF: break;
        }

      draw_object (player_object);
      draw_object (player2_object);

      for (size_t i = 0; i < game_state.objects_count; ++i)
        {
          draw_object (game_state.objects[i]);
        }

      glDisable (GL_DEPTH_TEST);
      glDisable (GL_CULL_FACE);
      glEnable (GL_BLEND);
      draw_network_interfaces ();
      if (game_state.show_console) draw_console ();
      if (game_state.chat) draw_chat ();

      for (u32 i = 0; i < ARRAY_LEN (game_state.level_times); ++i)
        {
          u32 time = game_state.level_times[i];
          if (time == (u32) -1) break;
          draw_level_time (time);
        }

      draw_level_time (game_state.current_level_time);
      draw_network_messages ();

      SDL_GL_SwapWindow (game_state.window);
    }

  return 0;
}


