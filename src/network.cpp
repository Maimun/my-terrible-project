/* Buffers and Strings
 *
 * Copyright (C) 2019-2020 LibTec & Stefan Tanev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define MSG_TYPE_CHAT 'c'
#define MSG_TYPE_POS  'p'


static String os_recv (Buffer buffer);
static u32    os_send (String data);


static void
push_network_message (String message_in)
{
  if (message_in.size > network.messages_buffer_p.size) return;

  String message = {(char *) network.messages_buffer_p.data, message_in.size + 2};
  network.messages_buffer_p = append_string (network.messages_buffer_p, message_in);
  network.messages_buffer_p = append_string (network.messages_buffer_p, STRING (" <"));

  if (network.messages_count == network.messages_max)
    {
      assert (network.messages_max);
      network.messages_max *= 2;
      // TODO: Thread Lock
      network.messages = REALLOC (network.messages, String, network.messages_max);
      // TODO: Thread Unlock
    }

  network.messages[network.messages_count++] = message;
}


static void
send_chat (String message)
{
  assert (message.size < network.send_data_buffer.size);

  Buffer p = network.send_data_buffer;
  p = append_char   (p, MSG_TYPE_CHAT);
  p = append_string (p, message);

  String data = {network.send_data_buffer.data, network.send_data_buffer.size - p.size};
  os_send (data);

  push_network_message (message);
}


static void
send_pos (V3 pos, r32 hangle)
{
  Buffer p = network.send_data_buffer;
  p = append_char   (p, MSG_TYPE_POS);
  p = append_string (p, {(char *) &pos, sizeof (pos)});
  p = append_string (p, {(char *) &hangle, sizeof (hangle)});

  String data = {network.send_data_buffer.data, network.send_data_buffer.size - p.size};
  os_send (data);
}


static void
start_network_connection ()
{
  network.bytes_recv = 0;
  network.bytes_sent = 0;

  char   buffer_mem[4096];
  Buffer buffer = {buffer_mem, sizeof (buffer_mem)};

  String data = os_recv (buffer);
  network.status = NETWORK_CONNECTED;

  while (1)
    {
      if (data.size == 0)
        {
          network.status = NETWORK_DISCONNECTED;
          break;
        }

      while (data.size)
        {
          int message_type = data.data[0];
          data.data++;
          data.size--;

          switch (message_type)
            {
            case MSG_TYPE_CHAT:
              {
                ++network.chats_recv_count;
                if (data.size)
                  {
                    push_network_message (data);
                    data.size = 0;
                  }
                else
                  {
                    ++network.invalid_messages_recv_count;
                  }
              } break;
            case MSG_TYPE_POS:
              {
                ++network.positions_recv_count;
                if (data.size >= sizeof (V3) + sizeof (r32))
                  {
                    char *p = data.data;
                    V3 pos = ((V3 *) p)[0];
                    p += sizeof (V3);
                    r32 hangle = ((r32 *) p)[0]; 
                    game_state.second_player_pos = pos;
                    game_state.second_player_hangle = hangle;
                    data.data += sizeof (V3) + sizeof (r32);
                    data.size -= sizeof (V3) + sizeof (r32);
                  }
                else
                  {
                    ++network.invalid_messages_recv_count;
                  }
              } break;
            default:
              {
                ++network.invalid_messages_recv_count;
              }
            }
        }

      data = os_recv (buffer);
    }
}
