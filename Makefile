prefix = /usr

VERSION = $(shell cat VERSION)
PACKAGES = sdl2 glew
CFLAGS = -Wall -Wno-unused-function -std=c++11
LDLIBS = -lm
MACROS =

ifdef ROOT_PATH
	MACROS += -DROOT_PATH=\"$(ROOT_PATH)\"
endif

ifeq ($(OS), Windows_NT)
   MACROS += -DOS_WINDOWS
   LDLIBS += -lopengl32 -mwindows -lWs2_32 -lIphlpapi
else
   MACROS += -DOS_GNULINUX
   LDLIBS += -pthread
   PACKAGES += gl
endif

CFLAGS += $(shell pkg-config --cflags $(PACKAGES))
LDLIBS += $(shell pkg-config --libs   $(PACKAGES))

3d: src/3d.cpp src/*.cpp
	g++ $(MACROS) $(CFLAGS) -o $@ $< $(LDLIBS)

.PHONY: deb
.ONESHELL:
deb:
	$(RM) -r deb
	mkdir -p deb/3d-$(VERSION)
	cp -a Makefile src res debian deb/3d-$(VERSION)
	cd deb
	sed -i 's/__VERSION__/'$(VERSION)'/' 3d-$(VERSION)/debian/changelog
	tar czf 3d_$(VERSION).orig.tar.gz 3d-$(VERSION)
	cd 3d-$(VERSION)
	debuild -us -uc
	cd ..
	mv 3d_$(VERSION)-1_amd64.deb ..
	cd ..
	$(RM) -r deb

.PHONY: install
install:
	mkdir -p $(DESTDIR)$(prefix)/bin
	mkdir -p $(DESTDIR)$(prefix)/share/3d/res
	mkdir -p $(DESTDIR)$(prefix)/share/3d/res/images
	mkdir -p $(DESTDIR)$(prefix)/share/3d/res/levels
	mkdir -p $(DESTDIR)$(prefix)/share/3d/res/models
	mkdir -p $(DESTDIR)$(prefix)/share/3d/res/shaders
	mkdir -p $(DESTDIR)$(prefix)/share/pixmaps
	mkdir -p $(DESTDIR)$(prefix)/share/applications
	install 3d   $(DESTDIR)$(prefix)/bin
	install res/* $(DESTDIR)$(prefix)/share/3d/res
	install res/images/* $(DESTDIR)$(prefix)/share/3d/res/images
	install res/levels/* $(DESTDIR)$(prefix)/share/3d/res/levels
	install res/models/* $(DESTDIR)$(prefix)/share/3d/res/models
	install res/shaders/* $(DESTDIR)$(prefix)/share/3d/res/shaders
	install res/3d-icon.xpm  $(DESTDIR)$(prefix)/share/pixmaps
	install res/3d.desktop   $(DESTDIR)$(prefix)/share/applications

.PHONY: clean
clean:
	$(RM) 3d
