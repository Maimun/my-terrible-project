#version 120

uniform mat4 transform_uni;
uniform float texcoord_xoffset_uni;

attribute vec2 position;
attribute vec2 texcoord;

varying vec2 texcoord_var;

void
main ()
{
  texcoord_var = texcoord;
  texcoord_var.x += texcoord_xoffset_uni;
  gl_Position = vec4 (position, 0, 1) * transform_uni;
}
