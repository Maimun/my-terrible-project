#version 120

uniform mat4 transform_uni;
uniform vec4 color_uni;

attribute vec3 position;
attribute vec3 normal;
attribute vec3 color;

void
main ()
{
  vec4 p = (vec4 (position, 1) * transform_uni);

  vec3 n = (vec4 (normal,   0) * transform_uni).xyz;
  n = normalize (n);

  vec3 light = normalize (vec3 (-1,  1, -1));
  float c = dot (n, light) * 0.5 + 0.5;

  gl_Position = p;
  gl_FrontColor = vec4 (color.xyz * c, 1);
}
