#version 120

uniform vec4 color_uni;
uniform sampler2D texture_uni;
varying vec2 texcoord_var;

void
main ()
{
  gl_FragColor.xyz = color_uni.xyz;
  gl_FragColor.  a = texture2D (texture_uni, texcoord_var).a;
}
